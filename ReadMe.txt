Welcome to my online code portfolio/samples. Hopefully these examples can help you get a better idea of my abilities and background. 
For the examples that come from my course work I have drawn from courses which provided little or no "starter code" so that 
they give a better impression of my own work. The folders represent individual courses or projects and contain ReadMes designed
to further explain the folder's contents. Anything not my own should be clearly marked with comments. Thank you.