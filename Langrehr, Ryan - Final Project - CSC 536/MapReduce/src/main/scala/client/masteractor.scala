package client

import com.typesafe.config.ConfigFactory
import akka.remote.routing.RemoteRouterConfig
import akka.actor.SupervisorStrategy._
import akka.remote.RemoteScope
import akka.actor.{Actor, Deploy, ActorRef, ActorPath, Props, Address, ActorSystem, AddressFromURIString, ActorLogging, OneForOneStrategy}
import scala.concurrent.duration._
import akka.routing.{Broadcast, RoundRobinPool, RoundRobinGroup, ConsistentHashingRoutingLogic, ConsistentHashingRouter, ConsistentHashingPool}
import akka.routing.ConsistentHashingRouter.ConsistentHashMapping
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.HashMap
import common._;

class MasterActor(plugin : Class[_]) extends Actor {

  //Reads in values for local and remote actors
  val numberMappers  = ConfigFactory.load.getInt("number-mappers")
  val numberReducers  = ConfigFactory.load.getInt("number-reducers")
  //Hahsmap for storing previously failed actors
  var failMap = new HashMap[ActorRef, Int]()
  var pending = numberReducers
  
  //Strategy checks if an actor has failed previously by storing failed actors in the hashamap and checking failures against that
  val strat = OneForOneStrategy(maxNrOfRetries = -1, withinTimeRange = Duration("Inf")){
    case _: Exception => if(failMap.contains(sender())){
                            failMap += (sender() -> (failMap(sender()) + 1))
                            Restart
                          }else{
                            failMap += (sender() -> 1)
                            Resume
                          }
      
  }

  //Address for use in remote
  val addresses = Seq(
    Address("akka", "MapReduceClient"),
    Address("akka.tcp", "server", "127.0.0.1", 2552)
  )

  //Consistent mapping
  def hashMapping: ConsistentHashMapping ={
    case ReducePair(key, value)=> key
  }

  //Spawns reduceActors
  var reduceActors = context.actorOf(RemoteRouterConfig(ConsistentHashingPool(numberReducers, hashMapping = hashMapping, supervisorStrategy = strat),addresses).props(Props(classOf[common.ReduceActor], plugin)))

  //Spawns mapActors
  val mapActors = context.actorOf(RemoteRouterConfig(RoundRobinPool(numberMappers, supervisorStrategy = strat),addresses).props(Props(classOf[common.MapActor], reduceActors, plugin)))


  def receive = {
    case msg: MapPair =>
      mapActors ! msg
    case Flush =>
      mapActors ! Broadcast(Flush)
    case Done =>
      pending -= 1
      if (pending == 0)
        context.system.terminate
  }
}
