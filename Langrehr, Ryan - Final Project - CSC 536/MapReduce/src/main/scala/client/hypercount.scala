package client

import common._
import scala.io.Source
import scala.util.matching.Regex
import scala.collection.mutable.HashMap
import scala.collection.mutable.ListBuffer
import java.net._
import java.io._

//Class for counting the number of incoming hyperlinks in a set of pages
@SerialVersionUID(400L)
class HyperCount extends Plugin with Serializable{
 type T= String
 type U= Int
  var reduceMap = new HashMap[String, Int]()//Storage for reduce results, key = page and val = link count

  def reduceProcess(message : ReducePair): HashMap[T, U]  = { 
   //function takes recieved links and tallies them
    val destination: String = (message.key).asInstanceOf[String]
    val origin: String = (message.value).asInstanceOf[String]
    //If the destination of the link has been encountered previously its count is increased
    if (reduceMap.contains(destination)) {
         reduceMap += (destination -> (1 + reduceMap(destination)))
    }
    //If not it is added to the map with a count of 1
    else{
	    reduceMap += (destination -> 1)
    }
    reduceMap  
  }
  
  def mapProcess(message : MapPair): List[ReducePair] = {
    //Retrieves links from the page and packages them for the reduce fucniton
    val page:String = (message.key).asInstanceOf[String]
    val source:String = (message.value).asInstanceOf[String]
    val html = Source.fromURL(source).mkString 
    var result: List[ReducePair] = List()
    
    for (url <- linkParser(page, html)){
      result = ReducePair(url.toString(), page) :: result
    }
    result    
  }
  //Prints results
  def printResult(){
    for((k,v) <- reduceMap){
      println(k + " : " + v.toString())
    }
  }
  //Parses the links, broken out from mapProcess largely for readability
  def linkParser(page : String, html: String): List[URL] ={
    var pageLinks: List[URL] = List()
    //Splits the string based on <a tag to find links
    for (linkSection <- (html.split("<a"))){
      //If this section of the page source does not begin with a link it is discarded
      if(linkSection.trim().startsWith("href=")){
        //Chops off the non like portion of the source
        var link = (linkSection.split(">"))(0)
        //Strips the link of unneeded characters
        link = link.replaceAll("\"","")
        link = (link.replace("href=",""))
        //checks if the link is relative or absolute if not builds an absolute path
        if(link.startsWith("http")){
           var url = new URL(link)
           pageLinks = url :: pageLinks
        }
        else{
           var localPath = new URL(page).getPath();
           var context = new URL(page.replace(localPath,""))
           var url = new URL(context,link)
           pageLinks = url :: pageLinks
        }        
      }
    }
    pageLinks
  }
}