package client;
import akka.actor.{ActorSystem, Props}
import com.typesafe.config.ConfigFactory
import java.io.FileNotFoundException
import common._;
import scala.reflect._
import scala.io.Source

object RemoteMapReduce extends App {
  val system = ActorSystem("MapReduceClient", ConfigFactory.load.getConfig("client"))
  
  //Menu to choose which plugin to use. In order to add another plugin an addition to this selection is ne
  println("Please enter the number for the plugin to run:")
  println("1. Dickens Name Counter")
  println("2. Text File Word Counter (With location of file transmitted to mapactor.)")
  println("3. Text File Word Counter (With text of file transmitted to mapactor.)")
  println("4. Link Counter")
  
  var selection = scala.io.StdIn.readLine()
   
  if(selection.equals("1")){
    var master = system.actorOf(Props(new MasterActor(classOf[Dickens])), name = "master")
    master ! MapPair("Dombey and Son", "http://reed.cs.depaul.edu/lperkovic/csc536/homeworks/gutenberg/pg883.txt")
    master ! MapPair("Oliver Twist", "http://reed.cs.depaul.edu/lperkovic/csc536/homeworks/gutenberg/pg821.txt")
    master ! MapPair("The Old Curiosity Shop", "http://reed.cs.depaul.edu/lperkovic/csc536/homeworks/gutenberg/pg730.txt")
    master ! MapPair("David Copperfield", "http://reed.cs.depaul.edu/lperkovic/csc536/homeworks/gutenberg/pg700.txt")
    master ! Flush
  }
     
  if(selection.equals("2")){
    var master = system.actorOf(Props(new MasterActor(classOf[WordCount])), name = "master")
    master ! MapPair("Poetics by Aristotle", "Poetics.txt")
    master ! MapPair("Phaedrus by Plato", "Phaedrus.txt")
    master ! MapPair("The Categories by Aristotle", "Categories.txt")
    master ! MapPair("Apology by Plato", "Apology.txt")
    master ! Flush
  }
  
  if(selection.equals("3")){
    var master = system.actorOf(Props(new MasterActor(classOf[WordCountSendText])), name = "master")
    try{
      master ! MapPair("Poetics by Aristotle", Source.fromFile("Poetics.txt").mkString)
    }catch{
      case ex: FileNotFoundException => println("One of the files does not exist or is inaccessable.")
    }  
    try{
      master ! MapPair("Phaedrus by Plato", Source.fromFile("Phaedrus.txt").mkString)
    }catch{
      case ex: FileNotFoundException => println("One of the files does not exist or is inaccessable.")
    }
    try{
      master ! MapPair("The Categories by Aristotle", Source.fromFile("Categories.txt").mkString)
    }catch{
      case ex: FileNotFoundException => println("One of the files does not exist or is inaccessable.")
    }
    try{
      master ! MapPair("Apology by Plato", Source.fromFile("Apology.txt").mkString)
    }catch{ 
      case ex: FileNotFoundException => println("One of the files does not exist or is inaccessable.")
    }
    master ! Flush
  }
  
  if(selection.equals("4")){
    var master = system.actorOf(Props(new MasterActor(classOf[HyperCount])), name = "master")
    master ! MapPair("http://reed.cs.depaul.edu/lperkovic/one.html", "http://reed.cs.depaul.edu/lperkovic/one.html")
    master ! MapPair("http://reed.cs.depaul.edu/lperkovic/two.html", "http://reed.cs.depaul.edu/lperkovic/two.html")
    master ! MapPair("http://reed.cs.depaul.edu/lperkovic/three.html", "http://reed.cs.depaul.edu/lperkovic/three.html")
    master ! MapPair("http://reed.cs.depaul.edu/lperkovic/four.html", "http://reed.cs.depaul.edu/lperkovic/four.html")
    master ! MapPair("http://reed.cs.depaul.edu/lperkovic/five.html", "http://reed.cs.depaul.edu/lperkovic/five.html")
    master ! Flush
  }
  

}
