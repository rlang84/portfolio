package client

import common.MapPair
import common.ReducePair
import common.Plugin
import scala.io.Source
import scala.util.matching.Regex
import scala.collection.mutable.HashMap
import scala.collection.mutable.ListBuffer

@SerialVersionUID(100L)
//This plugin class will count any names (capitalized words really) in a set a links.
class Dickens extends Plugin with Serializable{
    type T = String
    type U = ListBuffer[T]
    var reduceMap = new HashMap[String, ListBuffer[String]]() //reduce result storage key = name, val = list of books it appears in.
    
    def reduceProcess(message : ReducePair): HashMap[T, U]  = {
      //Groups titles with names
      val name:String = (message.key).asInstanceOf[String]    
      val title:String = (message.value).asInstanceOf[String]
      //Checks if the title has been seen already or not. If it has it adds the new title to the assosciated list. If not it ads the name and title.
      if (reduceMap.contains(name)) {
         reduceMap += (name -> (reduceMap(name) += title))
      }
      else{
	     reduceMap += (name -> ListBuffer(title))
      }
     reduceMap
    }

    def mapProcess(message : MapPair): List[ReducePair] = {
      //Extracts unique names from text and passes them along with the title name to the reduce funciton.
       val title:String = (message.key).asInstanceOf[String]
       val content:String = (message.value).asInstanceOf[String]
       //Regext to find capitalized words
       val capsFirst = "\\b[A-Z]+[a-z]+".r
       //Reads in the text
       val bookText = Source.fromURL(content).mkString
       //Extracts all names and removes duplicates
       val names = capsFirst.findAllIn(bookText).toList
       val uniqueNames = names.distinct
       //Result storage list
       var result: List[ReducePair] = List()
       //Packages and stores results as pairs
       for (unique <- uniqueNames){
         result = ReducePair(unique, title) :: result
       }
       result
    }
    //Prints results
    def printResult(){          
      for((k,v) <- reduceMap){
        println(k + " : " + v.mkString(", "))
      }
    }
}
