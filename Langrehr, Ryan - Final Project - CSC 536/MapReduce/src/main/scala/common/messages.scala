package common

case class MapPair(key : Object, value: Object)
case class ReducePair(key: Object, value: Object)
case object Flush
case object Done
