package common

import scala.io.Source
import scala.util.matching.Regex
import scala.collection.mutable.HashMap
import scala.collection.mutable.ListBuffer
import client._

//Abstract class to provide the interface for for all plugin classes. 
@SerialVersionUID(100L) 
abstract class Plugin extends Serializable{
  type T
  type U
  def reduceProcess(message : ReducePair): HashMap[T, U]  
  def mapProcess(message : MapPair): List[ReducePair]
  def printResult()
}
