package server;
import com.typesafe.config.ConfigFactory
import akka.actor.{Actor, ActorSystem, ActorRef}
import scala.collection.mutable.{HashMap, ListBuffer}
import scala.io.Source
import scala.util.matching.Regex
import common._

object Server extends App {
  val system = ActorSystem("server", ConfigFactory.load.getConfig("server"))
  println("Server ready")
}
