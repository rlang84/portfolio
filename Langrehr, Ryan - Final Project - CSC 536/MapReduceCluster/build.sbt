lazy val root = (project in file(".")).
settings(
    name := "ClusterMapReduce",
    version := "1.0",
    scalaVersion := "2.11.8",
    scalacOptions in ThisBuild ++= Seq("-unchecked", "-deprecation"),
    resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/",
    libraryDependencies ++= Seq("com.typesafe.akka" %% "akka-actor" % "2.4.7",
    "com.typesafe.akka" %% "akka-remote" % "2.4.7", "com.typesafe.akka" %% "akka-remote" % "2.4.7", "com.typesafe.akka" %% "akka-cluster" % "2.4.7","com.typesafe.akka" % "akka-cluster-metrics_2.11" % "2.4.7")
)
