package mapreducecluster

import akka.actor.Actor
import com.typesafe.config.ConfigFactory


class ReduceActor(plugClass : Class[Plugin]) extends Actor {
  //Generic reduce actor for plugin map-reduce. Takes a class that extends plugin as its only parameter
  //to do the actual processing. Actor primarily handles communication.
  var remainingMappers = ConfigFactory.load.getInt("number-mappers")
  val plugin : Plugin = plugClass.newInstance()
  //Instantiates a new instance of the plugin. I chose to use classes rather than objects to allow the plugin class to handle storing the data as well
  //rather than handling the conversion of various data types to pass them back to the Actor which became very complicated in my attempt.

  def receive = {
    case msg: ReducePair =>
      //calls the plugins reduceProcess function on each ReducePair message. The results are stored inside the plugin until the final flush message is recieved.
      plugin.reduceProcess(msg)
    case Flush =>
      remainingMappers -= 1
      if (remainingMappers == 0) {
        //Upon detecting that it is done the actor tells the plugin to release its results.
        plugin.printResult()
        context stop self
      }
    case _ =>
      println("I have no idea what this message is")
  }
}
