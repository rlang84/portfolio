package mapreducecluster

import scala.io.Source
import scala.util.matching.Regex
import scala.collection.mutable.HashMap
import scala.collection.mutable.ListBuffer
import java.net.URI


@SerialVersionUID(300L)
//This version of the word count differs only from the main version in that it expects the text itself to be passed to the map actor
//I expected this would be needed in the event actors did not have access to the files themselves.
class WordCountSendText extends Plugin with Serializable{
  type T= String
  type U= Int
  var reduceMap = new HashMap[String, Int]()
  def reduceProcess(message : ReducePair): HashMap[T, U]  = {
    val word:String = (message.key).asInstanceOf[String]
    val count:Int = (message.value).asInstanceOf[Int]
    if (reduceMap.contains(word)) {
         reduceMap += (word -> (reduceMap(word) + count))
      }
      else{
	     reduceMap += (word -> count)
      }
     reduceMap

  }

  def mapProcess(message : MapPair): List[ReducePair] = {
    val title:String = (message.key).asInstanceOf[String]
    val content:String = (message.value).asInstanceOf[String]
    var result: List[ReducePair] = List()

    for (word <- (content.toLowerCase.split("[\\p{Punct}\\s]+"))){
      result = ReducePair(word.trim(), Int.box(1)) :: result
    }
    result
  }

  def printResult(){
    for((k,v) <- reduceMap){
      println(k + " : " + v.toString())
    }
  }
}
