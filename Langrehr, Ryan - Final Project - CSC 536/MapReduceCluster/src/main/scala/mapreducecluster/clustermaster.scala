package mapreducecluster

import com.typesafe.config.ConfigFactory
import akka.cluster._
import akka.cluster.ClusterEvent._
import akka.cluster.routing.ClusterRouterPool
import akka.cluster.routing.ClusterRouterPoolSettings
import akka.actor.SupervisorStrategy._
import akka.actor.{Actor, Props}
import scala.concurrent.duration._
import akka.routing.{Broadcast, RoundRobinPool, ConsistentHashingPool}
import akka.routing.ConsistentHashingRouter.ConsistentHashMapping

class ClusterMaster(plugin : Class[_]) extends Actor {

  val cluster = Cluster(context.system)

  //Reads in values for local and remote actors
  val numberMappers  = ConfigFactory.load.getInt("number-mappers")
  val numberReducers  = ConfigFactory.load.getInt("number-reducers")
  var pending = numberReducers

  //Consistent mapping
  def hashMapping: ConsistentHashMapping ={
    case ReducePair(key, value)=> key
  }

  //Taken from the akka docs section on Akka Cluster Usage
  override def preStart(): Unit = {
  //#subscribe
  cluster.subscribe(self, initialStateMode = InitialStateAsEvents,
    classOf[MemberEvent], classOf[UnreachableMember])
  //#subscribe
}

  //Spawns reduceActors
  var reduceActors = context.actorOf(ClusterRouterPool(ConsistentHashingPool(numberReducers, hashMapping = hashMapping),ClusterRouterPoolSettings(totalInstances = 100, maxInstancesPerNode = 4,
    allowLocalRoutees = false, useRole = None)).props(Props(classOf[ReduceActor], plugin)))

  //Spawns mapActors
  val mapActors = context.actorOf(ClusterRouterPool(RoundRobinPool(numberMappers), ClusterRouterPoolSettings(totalInstances = 100, maxInstancesPerNode = 4,
    allowLocalRoutees = false, useRole = None)).props(Props(classOf[MapActor], reduceActors, plugin)))


  def receive = {
    case msg: MapPair =>
      mapActors ! msg
    case Flush =>
      mapActors ! Broadcast(Flush)
    case Done =>
      pending -= 1
      if (pending == 0)
        context.system.terminate
  }
}
