package mapreducecluster

import akka.actor.{Actor, ActorRef}
import akka.routing.Broadcast
import com.typesafe.config.ConfigFactory
import akka.actor.actorRef2Scala


class MapActor(reduceActors: ActorRef, plugClass : Class[Plugin]) extends Actor {
  //Generic map actor for plugin map-reduce. takes the usual ActorRef for reduce actors and a class that extends plugin
  //to do the actual processing. Actor primarily handles communication.
  val plugin : Plugin = plugClass.newInstance()
  //Instantiates a new instance of the plugin. I chose to use classes rather than objects to allow the plugin class to handle storing the data as well
  //rather than handling the conversion of various data types to pass them back to the Actor which became very complicated in my attempt.
  val numReducers = ConfigFactory.load.getInt("number-reducers")

  def receive = {
    case msg: MapPair =>
      //calls the plugins mapProcess funcion on the message and recieves a list of results as output. These are then sent to the reduceActors.
      var workList = plugin.mapProcess(msg)
      for(pair <- workList){
        reduceActors ! pair
      }
    case Flush =>
      reduceActors ! Broadcast(Flush)
  }
}
