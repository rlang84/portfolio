package mapreducecluster

import scala.collection.mutable.HashMap

//Abstract class to provide the interface for for all plugin class

@SerialVersionUID(100L)
abstract class Plugin extends Serializable{
  type T
  type U
  def reduceProcess(message : ReducePair): HashMap[T, U]
  def mapProcess(message : MapPair): List[ReducePair]
  def printResult()
}
