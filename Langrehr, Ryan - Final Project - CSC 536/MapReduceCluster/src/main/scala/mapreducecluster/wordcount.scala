package mapreducecluster

import scala.io.Source
import scala.util.matching.Regex
import scala.collection.mutable.HashMap
import scala.collection.mutable.ListBuffer
import java.net.URI


@SerialVersionUID(200L)
class WordCount extends Plugin with Serializable{
  //This version of the word count is inteded to be used when any actors have local availability of the file.
  type T= String
  type U= Int
  var reduceMap = new HashMap[String, Int]() //Stores results in the WordCount object itself rather than in the actor. key = word, val = count

  def reduceProcess(message : ReducePair): HashMap[T, U]  = {
    //Retrieves the data in the pair
    val word:String = (message.key).asInstanceOf[String]
    val count:Int = (message.value).asInstanceOf[Int]
    //Checks if the map already contains the word, if it does it increments the count for that word
    if (reduceMap.contains(word)) {
         reduceMap += (word -> (reduceMap(word) + count))
      }
    //If the word has never been seen it simply adds the word and its count to the map
    else{
     reduceMap += (word -> count)
    }
     reduceMap

  }

  def mapProcess(message : MapPair): List[ReducePair] = {
    //Processes the text striping out individual words and returning a list of results to the MapActor
    val title:String = (message.key).asInstanceOf[String]
    val file:String = (message.value).asInstanceOf[String]
    //Reads in the file
    val content = Source.fromFile(file).mkString
    //Holds a list of all words encountered.
    var result: List[ReducePair] = List()
    //Splits the text by word and removes punctuation. Then cycles through each word adding it to the result list
    for (word <- (content.toLowerCase.split("[\\p{Punct}\\s]+"))){
      result = ReducePair(word.trim(), Int.box(1)) :: result
    }
    result
  }

  //Prints the results of the results map
  def printResult(){
    for((k,v) <- reduceMap){
      println(k + " : " + v.toString())
    }
  }
}
