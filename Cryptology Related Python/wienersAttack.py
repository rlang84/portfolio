#Python implementation of Wiener's Attack on RSA

def confrac(e,n):
    #takes e and n and returns the continued fraction as a list of the a values
    cf = [] #list to store results
    remainder = e%n
    #runs till it hits an integer
    while remainder != 0:
        #handles a proper fraction
        if e < n:
            cf.append(0)
            #swaps e and n
            temp = e
            e = n
            n = temp
            continue
        #calcs a val and adds to list
        a = e//n
        cf.append(a)
        remainder = e%n
        #puts n on top and calculates new bottom
        temp = n
        n = e-n*a
        e = temp
    return cf

def converge(a):
    # takes a list of a values and then retuns the convergences as a list of (p, q) pairs
    pq = [(0,1),(1,0)] #primes the p and q values
    n = 0
    #while loop runs to end of a vals available
    while n < len(a):
        #calculates ps and qs
        p = a[n]*pq[n+1][0]+pq[n][0]
        q = a[n]*pq[n+1][1]+pq[n][1]
        pq.append((p,q))
        n = n + 1
    return pq

def quadratic(n, phiN):
    #takes n and phi(n) and returns p and q as a pair
    part1 = (n-phiN+1)//2
    part2 = sqrt(((part1**2))-n)
    p = part1+part2
    q = part1-part2
    #returns as a pair
    return (p,q)

def sqrt(m):
    #sqrt for integers based of the Heron's method discussed in class and the wikipedia section on the Babylonian Method, sloppy but seems to work
    x = 1
    #makes 100 guesses, 100 chosen arbitarially
    for i in range(100):
        x=(x+m//x)//2
        #catch if it manages to actually guess the right number
        if x**2 == m:
           return x
    return x

#def wienersAttack():#for testing
def wienersAttack(e,n):
    #Performs Wiener's attack givin e, n, and a number of attempts to make
    #e, n = 1249243, 1751669 #for testing
    #e, n = 7189933355424425793414895327, 14229868101208844783959742887 #for testing e,n
    a = confrac(e,n)
    pq = converge(a)
    limitcount = 0
    for pair in pq:
       k = pair[0] #p value
       d = pair[1] #q value
       #skips non-integer candiates candidates
       if k == 0 or d == 0:
           print(str(k) + "/" + str(d) + " is not feasable, k/d equals infinity or 0.")
           continue
       phiN = (e*d -1) // k #calculate phi(n)
       if(phiN//1 == phiN): #check if phi(n) is an integer
           if phiN < n: #check smaller than n
               quad = quadratic(n, phiN) #run through quadratic equation:
               if quad[0]*quad[1]==n: #final test to verufy that p*q=n, if passed prints result and exits loop
                print(str(k) + "/" + str(d) + ", d = " + str(d) + ", phiN = " + str(phiN) + " p= " + str(int(quad[0])) + " q= " + str(int(quad[1])) )
                break
           print(str(k) + "/" + str(d) + " phi(n)=" + str((e*d -1)//k) + " is not feasable.")
       else:
           print(str(k) + "/" + str(d) + " is not feasable, k/d is not an integer.")
