from fractions import gcd
from random import randint

#A python implementation of the Solovay/Strassen method for primality testing.

def solstra(n):
    #keeps going til it finds a prime
    while(True):
        #incriments n to the next value to check if prime
        n = n+1
        #catch evens
        if n%2 == 0:
            n = n+1
        #checks for 30 random a vals
        for i in range(0,30):
            #generates a new random a
            a = randint(1,n-1)
            # calcs a^(n-1/2)
            eul = expm(a,((n-1)/2),n)
            #skips to next n if not equal to 1 or n-1
            if eul != 1 and eul != n-1:
                break
            #calculates jacobi number
            jak = jacobi(a,n)
            #if outcomes not equal breaks and moves on to the next n val
            if jak%n != eul%n:
                break
            #if the candidate passes 30 tests returns it as a probable prime
            if i == 29:
                return str(n) + " is the next probable prime."


def jacobi(a,n):
    # Property 1
    a = a%n
    # No Zeros!
    if a == 0 or a%n == 0:
        return 0
    # Property 3
    if a == -1:
        return -1
    # Property 2
    if a == 1: #catches a=1
        return 1
    if a%2 == 0 and a != 2: #takes out factors of 2
        return jacobi(2,n) * jacobi(a//2,n)
    # Property 4
    if a == 2:
        x = n%8
        if x == 1 or x == 7:
            return 1
        if x == 3 or x== 5:
            return -1
    # Propery 5
    if gcd(a,n) == 1 and a % 2 != 0:
        if a%4 == n%4 == 3%4:
            return -1 * jacobi(n,a)
        else:
            return jacobi(n,a)
    else:
        return 0 #catch all, prevents running forever, I initially forgot to filter out even n's and encountred infinate recursion

#Taken from class materials.
def expm(a,x,n):
    if x < 0:
        return invm(expm(a,-x,n),n)
    else:
        product = 1
        power = a
        while x > 0:
            if x % 2 == 1:
                product = (product * power) % n
            power = (power*power) % n
            x = x//2
        return product
