n,r,p,q,c = 4321248229, 27348, 57251, 75479, 2269253365

#A small program designed to decrypt messages encrypted using the Rabin Cryptosystem.

def rabinDecrypt(n,r,p,q,c):
    #return all 0 if p and q are not = 3 (mod 4)
    if p%4 != 3 and q%4 != 3:
        return(0,0,0,0)
    #calculates section under the sqrt
    d = ((r*invm(2,n))**2+c)%n
    #perfoms the sqrt
    sp = expm(d,(p+1)//4,p)
    sq = expm(d,(q+1)//4,q)
    #calculates negative versions
    negsp = (sp*-1)%n
    negsq = (sq*-1)%n
    #calculates the potential ms
    m1 = (-r*invm(2,n)+chinese(sp,p,sq,q))%n
    m2 = (-r*invm(2,n)+chinese(negsp,p,sq,q))%n
    m3 = (-r*invm(2,n)+chinese(sp,p,negsq,q))%n
    m4 = (-r*invm(2,n)+chinese(negsp,p,negsq,q))%n
    return(m1,m2,m3,m4)

def chinese(a,m,b,n):
    #derived from p.77 of the text
    i = invm(n,m)
    k = ((a-b)*i)%m
    x = (b + n*k)%(m*n)
    return x

#All below is code from class
def invm(a,n):
    a = a % n
    a, b = n, a
    y0, y1 = 0, 1
    while (b > 0):
        q = a // b
        a, b = b, a - q*b
        y0, y1 = y1, y0 - q*y1
    y0 = y0 % n
    if a > 1:
        return
    return y0

def expm(a,x,n):
    'calculate a**x mod n'
    if x < 0:
        return invm(expm(a,-x,n),n)
    else:
        product = 1
        power = a
        while x > 0:
            if x % 2 == 1:
                product = (product * power ) % n
            power = (power*power) % n
            x = x//2
        return product
