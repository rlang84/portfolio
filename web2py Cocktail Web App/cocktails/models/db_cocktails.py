# -*- coding: utf-8 -*-
'''Contains the recipe ingredients, one entry for each recipe'''
db.define_table('ingredients', Field('ingredient_name', unique=True), Field('category'), format='%(ingredient_name)s')

'''Holds ther recipes names, instructions, and photo if there is one'''
db.define_table('recipes', Field('cocktail_name', unique=True), Field('ingredients_quantities', 'text'), Field('instructions', 'text'), Field('creator', 'reference auth_user', default=auth.user_id, writable=False, readable=False), Field('image', 'upload'), format='%(cocktail_name)s')

'''Holds a users ingredients, one entry for each user ingredient, manages many to many relationship'''
db.define_table('user_bar', Field('user_id', 'reference auth_user', default=auth.user_id, writable=False, readable=False), Field('ingredient', db.ingredients))

'''Manages many to many relstionship between recipes and ingredients'''
db.define_table('recipe_ingredients', Field('ingredients', db.ingredients), Field('recipes', db.recipes, writable=False, readable=False))

'''Holds the tags'''
db.define_table('tags', Field('tag_name', unique=True), format='%(tag_name)s')

'''Holds links to tags selected by a user'''
db.define_table('user_tags', Field('user_id', 'reference auth_user', writable=False, readable=False), Field('tags', db.tags, unique=True))

'''Representation of the many to many relationship between tags and recipes'''
db.define_table('recipe_tags', Field('tag', db.tags), Field('recipe', db.recipes))

'''Holds user slected favorites'''
db.define_table('user_favorites', Field('user_id', 'reference auth_user', default=auth.user_id, writable=False, readable=False), Field('recipe', db.recipes))

'''Holds the comments'''
db.define_table('comments', Field('recipe_id', 'reference recipes', writable=False, readable=False), Field('owner_id', 'reference auth_user', default = auth.user_id, writable=False, readable=False),Field('body', 'text'),Field('created_on', 'datetime', default=request.now))

'''Ingredient category restriction'''
db.ingredients.category.requires=IS_IN_SET(('Spirit', 'Liqueur', 'Mixer', 'Other'))
