# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
#########################################################################
''''''
def index():
    return dict()

'''This function displays an individual recipe'''
def show_recipe():
    cocktail = db.recipes(request.args(0, cast=int))
    db.comments.recipe_id.default = cocktail
    form = SQLFORM(db.comments).process() if auth.user else "Login to Comment"
    pagecomments = db(db.comments.recipe_id == cocktail).select(db.comments.owner_id, db.comments.created_on, db.comments.body)
    recipetags = db(db.recipe_tags.recipe == cocktail).select(db.recipe_tags.tag)
    return dict(cocktail=cocktail, form=form, pagecomments=pagecomments, recipetags=recipetags)

'''This function displays an alphabetical list of all recipes'''
def browse():
    masterlist=db(db.recipes).select(db.recipes.ALL, orderby=db.recipes.cocktail_name)
    return dict(masterlist=masterlist)

'''This function retrives all of the recipes a user has submitted'''
@auth.requires_login()
def my_recipes():
    recipe_list = db(db.recipes.creator == auth.user.id).select()
    return dict(recipe_list=recipe_list)

'''This function retrieves the users saved favorites'''
@auth.requires_login()
def my_favorites():
    recipe_list = db(db.user_favorites.user_id == auth.user.id).select()
    return dict(recipe_list=recipe_list)

'''Removes a favorite from the list'''
@auth.requires_login()
def remove_favorite():
    deletee = request.args(0, cast=int)
    db((db.user_favorites.user_id == auth.user.id) & (db.user_favorites.recipe == deletee)).delete()
    redirect(URL('my_favorites'))
    return dict()

'''Saves a recipe to favorites.'''
@auth.requires_login()
def save_recipe():
    recipe_id = request.args(0, cast=int)
    db.user_favorites.insert(user_id=auth.user.id, recipe=recipe_id)
    redirect(URL('show_recipe', args=recipe_id))

'''Creates a new recipe entry in the database with the name provided'''
@auth.requires_login()
def new_recipe_name():
    newRecipe = SQLFORM(db.recipes, fields=['cocktail_name']).process()
    if newRecipe.accepted:
        redirect(URL('new_cocktail_details', args=newRecipe.vars.id))
    return dict(newRecipe=newRecipe)

'''Removes and ingredient from the recipe page'''
@auth.requires_login()
def ingredient_remove():
    deletee = request.args(0, cast=int)
    recipe = request.args(1, cast=int)
    creatorcheck = db.recipes(recipe, creator=auth.user.id)
    if not creatorcheck:
        redirect(URL('index'))
    db((db.recipe_ingredients.ingredients == deletee) & (db.recipe_ingredients.recipes == recipe)).delete()
    redirect(URL('new_cocktail_details', args=recipe))
    return dict()

'''Allows the user to enter the details of a new recipe created in the function above'''
@auth.requires_login()
def new_cocktail_details():
    recipe_id = request.args(0, cast=int)
    creatorcheck = db.recipes(recipe_id, creator=auth.user.id)
    if not creatorcheck:
        redirect(URL('index'))
    db.recipes.id.readable = False
    recipe = db.recipes(id=recipe_id)
    db.recipes.id.default = recipe_id
    db.recipe_ingredients.recipes.default = recipe_id
    db.recipe_tags.recipe.default = recipe_id
    recipeForm = SQLFORM(db.recipes, recipe, fields=['ingredients_quantities', 'instructions', 'image']).process()
    tagform = SQLFORM(db.recipe_tags, fields=['tag']).process()
    recipetags = db(db.recipe_tags.recipe == recipe_id).select(db.recipe_tags.tag, db.recipe_tags.id)
    ingredientForm = SQLFORM(db.recipe_ingredients).process()
    ingredientlist = db(db.recipe_ingredients.recipes == recipe_id).select(db.recipe_ingredients.ingredients)
    if recipeForm.accepted:
        session.flash = 'Posted'
    if ingredientForm.accepted:
        session.flash = 'Added'
    return dict(tagform=tagform, recipetags=recipetags, recipe=recipe, recipeForm=recipeForm, ingredientForm=ingredientForm, ingredientlist=ingredientlist)

'''This function allows users to manage their own bar.'''
@auth.requires_login()
def my_bar():
    cat = request.args(0)
    db.user_bar.ingredient.requires = IS_IN_DB(db(db.ingredients.category == cat), 'ingredients.id', '%(ingredient_name)s')
    myingredient = SQLFORM(db.user_bar).process()
    if myingredient.accepted:
        session.flash = 'Added'
    myingredients = db((db.user_bar.user_id == auth.user.id)&(db.user_bar.ingredient == db.ingredients.id)).select(db.user_bar.id, db.ingredients.ingredient_name)
    return dict(myingredient=myingredient, myingredients=myingredients)

'''This removes an ingredient from a users bar'''
@auth.requires_login()
def bar_remove():
    deletee = request.args(0, cast=int)
    db((db.user_bar.user_id == auth.user.id) & (db.user_bar.id == deletee)).delete()
    redirect(URL('my_bar'))
    return dict()

'''Adds tags to a search'''
@auth.requires_login()
def add_tags():
    db.user_tags.user_id.default = auth.user.id
    searchparams = SQLFORM(db.user_tags).process()
    alltags = db(db.user_tags.user_id==auth.user.id).select(db.user_tags.id, db.user_tags.tags)
    if searchparams.accepted:
        session.flash = 'Added'
    return dict(searchparams=searchparams, alltags=alltags)

'''This function retrieves recipes based upon the user's bar'''
@auth.requires_login()
def get_recipies():
    count = db.recipe_ingredients.recipes.count()
    barset = db(db.recipe_ingredients.ingredients == db.user_bar.ingredient).select(db.recipe_ingredients.recipes, count, groupby=db.recipe_ingredients.recipes)
    allrecipes = db(db.recipe_ingredients.recipes).select(db.recipe_ingredients.recipes, count, groupby=db.recipe_ingredients.recipes, having=db.recipe_ingredients.recipes)
    return dict(allrecipes=allrecipes, barset=barset, count=count)

'''This function retrieves recipes based upon a user's bar and tags they select and save'''
@auth.requires_login()
def get_recipes_tags():
    count = db.recipe_ingredients.recipes.count()
    searchparams = SQLFORM(db.user_tags).process()
    alltags = db(db.user_tags.tags == db.recipe_tags.tag)._select(db.recipe_tags.recipe)
    barset = db(db.recipe_ingredients.ingredients == db.user_bar.ingredient).select(db.recipe_ingredients.recipes, count, groupby=db.recipe_ingredients.recipes)
    allrecipes = db(db.recipe_ingredients.recipes.belongs(alltags)).select(db.recipe_ingredients.recipes, count, groupby=db.recipe_ingredients.recipes, having=db.recipe_ingredients.recipes)
    return dict(allrecipes=allrecipes, barset=barset, count=count, alltags=alltags, searchparams=searchparams)

'''Searches recipes based upon a users saved tags'''
@auth.requires_login()
def search_by_tag():
    db.user_tags.user_id.default = auth.user.id
    searchparams = SQLFORM(db.user_tags).process()
    alltags = db(db.user_tags).select(db.user_tags.id, db.user_tags.tags)
    if searchparams.accepted:
        session.flash = 'Added'
        alltags = db(db.user_tags.user_id==auth.user.id).select(db.user_tags.id, db.user_tags.tags)
    results = db((db.recipe_tags.tag == db.user_tags.tags) & (db.user_tags.user_id == auth.user.id)).select()
    return dict(searchparams=searchparams, results=results, alltags=alltags)

'''Removes tags from a search.'''
@auth.requires_login()
def tag_remove():
    deletee = request.args(0, cast=int)
    db((db.user_tags.user_id == auth.user.id) & (db.user_tags.id == deletee)).delete()
    redirect(URL('search_by_tag'))
    return dict()

'''Removes a tag from the users selection'''
@auth.requires_login()
def recipe_tag_remove():
    deletee = request.args(0, cast=int)
    recipe = request.args(1, cast=int)
    creatorcheck = db.recipes(recipe, creator=auth.user.id)
    if not creatorcheck:
        redirect(URL('index'))
    db((db.recipe_tags.id == deletee)).delete()
    redirect(URL('new_cocktail_details', args=recipe))
    return dict()

'''Clears all the tags save by a user'''
@auth.requires_login()
def drop_all_tags():
    db(db.user_tags.user_id == auth.user.id).delete()
    redirect(URL('search_by_tag'))
    return dict()

def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/manage_users (requires membership in
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()
