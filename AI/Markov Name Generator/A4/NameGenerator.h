#ifndef NameGenerator_H
#define NameGenerator_H

#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <cstdlib>
#include <time.h>

using namespace std;

bool statCom(const std::pair<string, double>& a, const std::pair<string, double>& b) {
	return a.second < b.second;
};

class NameGenerator {
public:

	NameGenerator(string gender, int minLen, int maxLen, int numNames);
	~NameGenerator();
	void run();

private:
	string alpha = "abcdefghijklmnopqrstuvwxyz_"; //alphabet for building pairs
	string girlFile = "namesGirls.txt"; //file names
	string boyFile = "namesBoys.txt";
	string usedFile; //stores one of the two above based on user input
	size_t minLen; //min and max name lengths
	size_t maxLen;
	int numNames; //number of names
	unordered_map<string, float> nameStats; //stores probabilities for pairs
	unordered_set<string> allNames; //stores all read in names and already generated names
	int runflag = 0; //determines if initiall set up functions still need to be run

	vector<string> nameGetter();
	void statCalc(vector<string> nameList);
	void setBuilder(vector<string> a);
	string nameBuilder();
};

#endif
