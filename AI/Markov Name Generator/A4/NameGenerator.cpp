#define _CRT_SECURE_NO_WARNINGS
#include "NameGenerator.h"

NameGenerator::NameGenerator(string gender, int minLen, int maxLen, int numNames){
	//constrcutor

	//chooses which filename to use based on the gender
	if (gender == "boy") {
		this->usedFile = "namesBoys.txt";
	}
	if (gender == "girl") {
		this->usedFile = "namesGirls.txt";
	}
	//adjusts for the _ at the begining and end.
	this->minLen = minLen + 2;
	this->maxLen = maxLen + 2;
	this->numNames = numNames;
}

NameGenerator::~NameGenerator() {
	//destructor
}

void NameGenerator::run(){
	//Sets up the generator and then runs it the appropriate number of times.

	//If run has not been run yet it calls the setup functions
	if (this->runflag == 0) {
		vector<string> names = this->nameGetter();
		this->setBuilder(names);
		this->statCalc(names);
		runflag = 1;
	}

	string newName = "";
	int nameTotal = 0;

	//While loop runs until number of names requested is output.
	while(nameTotal < numNames) {
		newName = nameBuilder();
		if (this->allNames.count(newName) == 0) {
			
			//adds new name to the list of already seen names.
			allNames.emplace(newName); 
			
			//Reformats name:
			newName.erase(newName.begin());
			newName.erase(newName.end() - 1);
			transform(newName.begin(), newName.begin() + 1, newName.begin(), ::toupper);
			nameTotal += 1;
			cout << "\n" << newName << "\n";
		}
	} 
}

vector<string> NameGenerator::nameGetter() {
	//Reads in the names from the file.

	vector<string> names;
	ifstream readIn;
	readIn.open(this->usedFile);
	while (!readIn.eof()) {
		string tmpString;
		getline(readIn, tmpString, '\n');
		transform(tmpString.begin(), tmpString.end(), tmpString.begin(), ::tolower);
		tmpString = "__" + tmpString + "__";
		names.push_back(tmpString);
	}
	return names;
}

void NameGenerator::statCalc(vector<string> nameList) {
	//Calculates the statiscial information for the name generation

	unordered_map<string, float> pairCount; //total number of each letter parings
	unordered_map<string, float> lettercount; //total number of each letter

	//Nested loops run through each letter of the alphabet to create the letter pairs
	for (size_t i = 0; i < nameList.size(); i++) {
		string name = nameList[i];
		for (size_t j = 0; j < name.size()-1; j++) {
			string letters = "";
			letters += name[j];
			letters += name[j + 1];
			//SKips the first and last "_"
			if (letters == "__") {
				continue;
			}
			//All other letters have their counts tracked here.
			else{
				string letter = "";
				letter += letters[0];
				lettercount[letter] += 1.0f;
				pairCount[letters] += 1.0f;
			}
		}
	}
	//Divides the pair by the total number of times the lead letter is encountered to derive the percentage of times that letter is followed by the second letter.
	for (auto pair = pairCount.begin(); pair != pairCount.end(); pair++) {
		string firstLetter = "";
		firstLetter += pair->first[0];
		float tempNum = (pair->second / lettercount[firstLetter]);
		nameStats[pair->first] = tempNum;
	}
	//Copies each pair by lead letter in order to replace the probabilities with a cumulative probability to facilitate selection with a pseudo-random number generator
	for (int i = 0; i < 27; i++) {
		vector<pair<string, float>> tempStats;
		for (int j = 0; j < 27; j++) {
			string searchKey = "";
			searchKey += alpha[i];
			searchKey += alpha[j];
			pair <string, float> search (searchKey, nameStats[searchKey]);
			tempStats.push_back(search);
		}
		//Sorts the letter's pairs and then replaces the probabilities with teh cumulative probability
		sort(tempStats.begin(), tempStats.end(), statCom);
		float cumulator = 0.0f;
		for (auto pair = tempStats.begin(); pair != tempStats.end(); pair++) {
			cumulator += pair->second;
			pair->second = cumulator;
			this->nameStats[pair->first] = cumulator;
		}
	}
}

void NameGenerator::setBuilder(vector<string> a) {
	//Constructs a hash out of the name input to allow for quick checking of novelty
	for (size_t i = 0; i < a.size(); i++) {
		this->allNames.emplace(a[i]);
	}
}

string NameGenerator::nameBuilder() {
	//funciton acutally builds the names
	string newName = "";
	//Seeds random num with system time, it seems that using the time does make it select similar starting letters when run within a short time of previous runs.
	srand((unsigned int)time(NULL));
	do {
		newName = "";
		string prevLetter = "_";
		string candidate = "";
		float candVal = 1.1f;	
		//Runs until broken by a closing "_"
		while(true) {
			//gets random float between 0 and 1
			float random = rand() / (float)RAND_MAX;
			for (int i = 0; i < 27; i++) {
				//builds pair to test
				string testLetter = prevLetter;
				testLetter += alpha[i];
				//gets probability of that pair
				float testVal = this->nameStats[testLetter];
				//tests to see if the rand is in the appropriate probability range
				if (testVal > random && testVal < candVal) {
					//updates temporary values to track best succession candidate
					candVal = testVal;
					candidate = alpha[i];
				}
			}
			//After finding the best candidate the letter is added to the name and the variables are updated and reset for the next round.
			newName += prevLetter;
			prevLetter = candidate;
			candVal = 1.1f;
			//Breaks teh loop if it gets to a closing "_"
			if (prevLetter == "_") {
				newName += prevLetter;
				break;
			}
		}
	//Stops if a name of the appropriate lenght is found.
	} while ((newName.length() < this->minLen) || (newName.length() > this->maxLen));
	return newName;
}

void main() {
	string gender;
	int min;
	int max;
	int num;
	cout << "Please type \"boy\" or \"girl\" to select the gender of the name.";
	cin >> gender;
	cout << "Please enter a minimum name length.";
	cin >> min;
	cout << "Please enter a maximum name length.";
	cin >> max;
	cout << "Please enter the number of names you would like to generate.";
	cin >> num;
	NameGenerator *babyNames = new NameGenerator(gender, min, max, num);
	babyNames->run();
	delete(babyNames);
}
