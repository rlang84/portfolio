import java.io.FileNotFoundException;
import java.util.*;

public final class Searches {

	static Puzzle solution = new Puzzle(4, 1, 2, 3, 8, 0, 4, 7, 6, 5);

	public static void breadthFirst(Puzzle startState) throws FileNotFoundException {
		//Breadth first search of the tree.
		Queue<Node> toVisit = new LinkedList<Node>(); 		//Queues for the nodes:
		HashSet <Integer> seen = new HashSet<Integer>(); 
		Node currentnode = new Node(startState);	//Initial setup.
		
		//Bookkeeping values
		int nodePop = 0;
		int maxQueue = 0;

		seen.add(Integer.parseInt(currentnode.toString()));
		while (currentnode != null) { //Iterates through the nodes on the toVisit queue.	
			if(solutionCheck(currentnode)){ //Checks for solution.
				System.out.printf("\nTime = %d; Space = %d\n", nodePop, maxQueue);
				break;
			}
			currentnode.expand();				
			for(Node childnode:currentnode.children){ //Iterates through the children of each node.
				if (childnode != null) {				
					if (seen.contains(Integer.parseInt(childnode.toString())) == false){
						toVisit.add(childnode); //Adds children to the end of the queue.
						seen.add(Integer.parseInt(childnode.toString()));
					}
				}
			}
			if(toVisit.isEmpty() == false){ //Gets the next node and pops it from the queue.
				currentnode = toVisit.remove();
				//Trackers for Time and Space
				nodePop+=1;
				int qSize = toVisit.size()+1;
				if(qSize > maxQueue){
					maxQueue = qSize;
				}
			}
			else{
				System.out.printf("No solution found.\n");
				break;
			}
		}
	}

	public static void depthFirst(Puzzle startState) throws FileNotFoundException {
		//Depth first search of the tree.
		Stack<Node> toVisit = new Stack<Node>(); 		//Queues for the nodes:
		HashSet <Integer> seen = new HashSet<Integer>(); 
		Node currentnode = new Node(startState);	//Initial setup.
		
		//Bookkeeping values
		int nodePop = 0;
		int maxQueue = 0;

		seen.add(Integer.parseInt(currentnode.toString()));
		while (currentnode != null) { //Iterates through the nodes on the toVisit queue.	
			if(solutionCheck(currentnode)){ //Checks for solution.
				System.out.printf("\nTime = %d; Space = %d\n", nodePop, maxQueue);
				break;
			}
			currentnode.expand();				
			for(Node childnode:currentnode.children){ //Iterates through the children of each node.
				if (childnode != null) {				
					if (seen.contains(Integer.parseInt(childnode.toString())) == false){
						toVisit.push(childnode); //Adds children to the end of the queue.
						seen.add(Integer.parseInt(childnode.toString()));
					}
				}
			}
			if(toVisit.isEmpty() == false){ //Gets the next node and pops it from the queue.
				currentnode = toVisit.pop();
				//Trackers for Time and Space
				nodePop+=1;
				int qSize = toVisit.size()+1;
				if(qSize > maxQueue){
					maxQueue = qSize;
				}
			}
			else{
				System.out.printf("No solution found.\n");
				break;
			}
		}
	}

	public static void iterativeDeep(Puzzle startState) throws FileNotFoundException {
		//Iterative Deepening search of the tree.
		
		//Bookkeeping values
		int nodePop = 0;
		int maxQueue = 0;
		
		int iterationDepth = 31; //Set to not go past 31, I arrived at this number because I used the incorrect solution at first and my breadth first search seemd to bottom out at this level.
		for(int depth = 0; depth < iterationDepth; depth++){
			Stack<Node> toVisit = new Stack<Node>(); 		//Queues for the nodes:
			HashSet <Integer> seen = new HashSet<Integer>(); 
			Node currentnode = new Node(startState);	//Initial setup.
			
			seen.add(Integer.parseInt(currentnode.toString()));
			while (currentnode != null ) { //Iterates through the nodes on the toVisit queue.	
				if(solutionCheck(currentnode)){ //Checks for solution.
					System.out.printf("\nTime = %d; Space = %d\n", nodePop, maxQueue);
					return;
				}
				currentnode.expand();				
				for(Node childnode:currentnode.children){ //Iterates through the children of each node.
					if (childnode != null) {
						if(childnode.depth > depth){
							break;
						}
						if (seen.contains(Integer.parseInt(childnode.toString())) == false){

							toVisit.push(childnode); //Adds children to the end of the queue.
							seen.add(Integer.parseInt(childnode.toString()));
						}
					}
				}
				if(toVisit.isEmpty() == false){ //Gets the next node and pops it from the queue.
					currentnode = toVisit.pop();
					//Trackers for Time and Space
					nodePop+=1;
					int qSize = toVisit.size()+1;
					if(qSize > maxQueue){
						maxQueue = qSize;
					}
				}
				else{
					System.out.printf("No solution found.\n");
					break;
				}
			}
		}

	}

	public static void uniformCost(Puzzle startState) throws FileNotFoundException {
		//Uniform-cost search of the tree.
		PriorityQueue<Node> toVisit = new PriorityQueue<Node>(); 		//Priority Queue for the nodes:
		HashMap <Integer, Integer> seen = new HashMap<Integer, Integer>(); 
		Node currentnode = new Node(startState);	//Initial setup.
		
		//Bookkeeping values
		int nodePop = 0;
		int maxQueue = 0;

		seen.put(Integer.parseInt(currentnode.toString()), currentnode.totalcost);
		while (currentnode != null) { //Iterates through the nodes on the toVisit queue.
			Integer keyInt = Integer.parseInt(currentnode.toString());
			boolean keyTest = seen.containsKey(keyInt);
			if(keyTest == true){ //Verifies that a lower cost version of this state was not found after this node's state was recorded. If that is the case the node is skipped.
				if(currentnode.cost > seen.get(keyInt)){
					continue;
				}
			}
			if(solutionCheck(currentnode)){ //Checks for solution.
				System.out.printf("\nTime = %d; Space = %d\n", nodePop, maxQueue);
				break;
			}
			currentnode.expand();
			currentnode.childSort();//Sorts nodes based on costs
			for(Node childnode:currentnode.childSorted){ //Iterates through the children of each node in order of cost.
				if (childnode != null) {
					keyInt = Integer.parseInt(childnode.toString());//I retrieve and save these values in this search since they will be used multiple times.
					keyTest = seen.containsKey(keyInt);
					if (keyTest == false){ //Adds children to the end of the queue, if the block pattern has not been seen before.
						toVisit.add(childnode); 
						seen.put(keyInt, childnode.totalcost);
					}
					if (keyTest == true){ //Checks to see if the key is lower cost and replaces it if it is.
						int oldval = seen.get(keyInt);
						if(oldval > childnode.totalcost)
							toVisit.add(childnode); //Adds children to the priority queue.
						seen.put(keyInt, childnode.totalcost);
					}
				}
			}
			if(toVisit.isEmpty() == false){ //Gets the next node and pops it from the queue.
				currentnode = toVisit.poll();
				//Trackers for Time and Space
				nodePop+=1;
				int qSize = toVisit.size()+1;
				if(qSize > maxQueue){
					maxQueue = qSize;
				}
			}
			else{
				System.out.printf("No solution found.\n");
				break;
			}
		}
	}

	public static void bestFirst(Puzzle startState) throws FileNotFoundException {
		//Best-first search of the tree.
		PriorityQueue<hNode> toVisit = new PriorityQueue<hNode>(); 		//Queues for the hNodes:
		HashMap <Integer, hNode> seen = new HashMap<Integer, hNode>();
		hNode currentnode = new hNode(startState);	//Initial setup.
		
		//Bookkeeping values
		int nodePop = 0;
		int maxQueue = 0;

		currentnode.greedyH();
		while (currentnode != null) { //Iterates through the hNodes on the toVisit queue.	
			if(solutionCheck(currentnode)){ //Checks for solution.
				System.out.printf("\nTime = %d; Space = %d\n", nodePop, maxQueue);
				break;
			}
			seen.put(Integer.parseInt(currentnode.toString()), currentnode);
			currentnode.expand();
			for(hNode childhNode:currentnode.children){ //Iterates through the children of each hNode.
				if (childhNode != null) {
					childhNode.greedyH(); //Sets the hvalue of the child node.
					Integer childkey = Integer.parseInt(childhNode.toString());
					if (seen.containsKey(childkey) == false){
						toVisit.add(childhNode); //Adds children to the end of the queue.
					}
					else if(seen.containsKey(childkey) == true){ //If the child's state is found in the seen list, its hval is checked and if it is > than the child, the child is swaped in.
						hNode oldnode = seen.get(childkey);
						if(oldnode.hval > childhNode.hval){
							toVisit.add(oldnode);
						}					
					}
				}
			}
			if(toVisit.isEmpty() == false){ //Gets the next hNode and pops it from the queue.
				currentnode = toVisit.remove();
				//Trackers for Time and Space
				nodePop+=1;
				int qSize = toVisit.size()+1;
				if(qSize > maxQueue){
					maxQueue = qSize;
				}
			}
			else{
				System.out.printf("No solution found.\n");
				break;
			}
		}
	}

	public static void A1(Puzzle startState) throws FileNotFoundException {
		//A* search of the tree using the number of in place blocks plus the total distance covered as its heuristic.
		PriorityQueue<hNode> toVisit = new PriorityQueue<hNode>(); 		//Queues for the hNodes:
		HashMap <Integer, hNode> seen = new HashMap<Integer, hNode>();
		hNode currentnode = new hNode(startState);	//Initial setup.
		
		//Bookkeeping values
		int nodePop = 0;
		int maxQueue = 0;

		currentnode.aH(); //Sets the havalue of the child node
		while (currentnode != null) { //Iterates through the hNodes on the toVisit queue.	
			if(solutionCheck(currentnode)){ //Checks for solution.
				System.out.printf("\nTime = %d; Space = %d\n", nodePop, maxQueue);
				break;
			}
			seen.put(Integer.parseInt(currentnode.toString()), currentnode);
			currentnode.expand();
			for(hNode childhNode:currentnode.children){ //Iterates through the children of each hNode.
				if (childhNode != null) {
					childhNode.aH(); //Sets the hvalue of the child node.
					Integer childkey = Integer.parseInt(childhNode.toString());
					if (seen.containsKey(childkey) == false){
						toVisit.add(childhNode); //Adds children to the end of the queue.
					}
					else if(seen.containsKey(childkey) == true){ //If the child's state is found in the seen list, its hval is checked and if it is > than the child, the child is swaped in.
						hNode oldnode = seen.get(childkey);
						if(oldnode.hval > childhNode.hval){
							toVisit.add(oldnode);
						}					
					}
				}
			}
			if(toVisit.isEmpty() == false){ //Gets the next hNode and pops it from the queue.
				currentnode = toVisit.remove();
				//Trackers for Time and Space
				nodePop+=1;
				int qSize = toVisit.size()+1;
				if(qSize > maxQueue){
					maxQueue = qSize;
				}
			}
			else{
				System.out.printf("No solution found.\n");
				break;
			}
		}
	}

	public static void A2(Puzzle startState) throws FileNotFoundException {
		//A* search of the tree using the number of in place blocks plus the total distance covered as its heuristic.
		PriorityQueue<hNode> toVisit = new PriorityQueue<hNode>(); 		//Queues for the hNodes:
		HashMap <Integer, hNode> seen = new HashMap<Integer, hNode>();
		hNode currentnode = new hNode(startState);	//Initial setup.
		
		//Bookkeeping values
		int nodePop = 0;
		int maxQueue = 0;

		currentnode.manhattanH();
		while (currentnode != null) { //Iterates through the hNodes on the toVisit queue.	
			if(solutionCheck(currentnode)){ //Checks for solution.
				System.out.printf("\nTime = %d; Space = %d\n", nodePop, maxQueue);
				break;
			}
			seen.put(Integer.parseInt(currentnode.toString()), currentnode);
			currentnode.expand();
			for(hNode childhNode:currentnode.children){ //Iterates through the children of each hNode.
				if (childhNode != null) {
					childhNode.manhattanH(); //Sets the hvalue of the child node.
					Integer childkey = Integer.parseInt(childhNode.toString());
					if (seen.containsKey(childkey) == false){
						toVisit.add(childhNode); //Adds children to the end of the queue.
					}
					else if(seen.containsKey(childkey) == true){ //If the child's state is found in the seen list, its hval is checked and if it is > than the child, the child is swaped in.
						hNode oldnode = seen.get(childkey);
						if(oldnode.hval > childhNode.hval){
							toVisit.add(oldnode);
						}					
					}
				}
			}
			if(toVisit.isEmpty() == false){ //Gets the next hNode and pops it from the queue.
				currentnode = toVisit.remove();
				//Trackers for Time and Space
				nodePop+=1;
				int qSize = toVisit.size()+1;
				if(qSize > maxQueue){
					maxQueue = qSize;
				}
			}
			else{
				System.out.printf("No solution found.\n");
				break;
			}
		}
	}

	public static void A3(Puzzle startState) throws FileNotFoundException {
		//A* search of the tree using the number of in place blocks plus the total distance covered as its heuristic.
		PriorityQueue<hNode> toVisit = new PriorityQueue<hNode>(); 		//Queues for the hNodes:
		HashMap <Integer, hNode> seen = new HashMap<Integer, hNode>();
		hNode currentnode = new hNode(startState);	//Initial setup.
		
		//Bookkeeping values
		int nodePop = 0;
		int maxQueue = 0;

		currentnode.customH();
		while (currentnode != null) { //Iterates through the hNodes on the toVisit queue.	
			if(solutionCheck(currentnode)){ //Checks for solution.
				System.out.printf("\nTime = %d; Space = %d\n", nodePop, maxQueue);
				break;
			}
			seen.put(Integer.parseInt(currentnode.toString()), currentnode);
			currentnode.expand();
			for(hNode childhNode:currentnode.children){ //Iterates through the children of each hNode.
				if (childhNode != null) {
					childhNode.customH(); //Sets the hvalue of the child node.
					Integer childkey = Integer.parseInt(childhNode.toString());
					if (seen.containsKey(childkey) == false){
						toVisit.add(childhNode); //Adds children to the end of the queue.
					}
					else if(seen.containsKey(childkey) == true){ //If the child's state is found in the seen list, its hval is checked and if it is > than the child, the child is swaped in.
						hNode oldnode = seen.get(childkey);
						if(oldnode.hval > childhNode.hval){
							toVisit.add(oldnode);
						}					
					}
				}
			}
			if(toVisit.isEmpty() == false){ //Gets the next hNode and pops it from the queue.
				currentnode = toVisit.remove();
				//Trackers for Time and Space
				nodePop+=1;
				int qSize = toVisit.size()+1;
				if(qSize > maxQueue){
					maxQueue = qSize;
				}
			}
			else{
				System.out.printf("No solution found.\n");
				break;
			}
		}
	}

	public static boolean solutionCheck(Node candidate){
		//Checks if a node's state is the solution.
		boolean ans = false;
		if (candidate.state.equals(solution) == true) {
			System.out.printf("We found it!\n");
			Searches.traceback(candidate);
			ans = true;
		}
		return ans;
	}

	public static void traceback(Node end) {
		//Traces back and prints out the nodes from a solution.
		Node step = end;
		while (step != null) {
			//System.out.printf("Moved: %s, Cost: %d\n", step.move, step.cost);
			step.printState();
			System.out.printf("\n");
			step = step.parent;
		}
	}

	public static boolean solutionCheck(hNode candidate){
		//hNode version of above.
		boolean ans = false;
		if (candidate.state.equals(solution) == true) {
			System.out.printf("We found it!\n");
			Searches.traceback(candidate);
			ans = true;
		}
		return ans;
	}

	public static void traceback(hNode end) {
		//hNode version of above.
		hNode step = end;
		while (step != null) {
			//System.out.printf("Moved: %s, Cost: %d\n", step.move, step.cost);
			step.printState();
			System.out.printf("\n");
			step = step.parent;
		}
	}
}
