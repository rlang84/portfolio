import java.util.Arrays;
import java.util.Comparator;

public class Node implements Comparator<Node>, Comparable<Node>{
	public
	Puzzle state = new Puzzle();
	int depth = 0;
	int cost = 0;
	int totalcost = 0;
	boolean expanded = false;
	boolean childFlag = false;
	boolean sortFlag = false;
	Node parent = null;
	Node[] children = {null, null, null, null};
	Node[] childSorted = {null, null, null, null};
	Action move = Action.start;

	Node(){
		//Default constructor creates a generic node.
	};

	Node(Puzzle stateT, int depthT, Node parentT, Action moveT, int costT){ 
		//Constructor for child nodes.
		this.state = stateT;
		this.depth = depthT; 
		this.parent = parentT; 
		this.move = moveT; 
		this.cost = costT; 
		this.expanded = false; 
		if(this.parent != null){
			this.totalcost = costT + parent.totalcost;
		}
		else{
			this.totalcost = costT;
		}
	}

	Node(Puzzle stateT) {
		//Constructor for a top node
		this.state = stateT; 
	}

	void expand() {
		//Function to expand child nodes, checks the cost of a move (0= not possible) and then performs the possible moves.
		this.expanded = true;
		int leftCost = this.state.costleft();
		int rightCost = this.state.costright();
		int upCost = this.state.costup();
		int downCost = this.state.costdown();

		if(leftCost != 0){
			Puzzle childstate = new Puzzle(this.state);
			childstate.mvleft();
			this.children[0] = new Node(childstate, this.depth+1, this, Action.left, leftCost);
			this.childFlag = true;
		}

		if(rightCost != 0){
			Puzzle childstate = new Puzzle(state);
			childstate.mvright();
			this.children[1] = new Node(childstate, this.depth + 1, this, Action.right, rightCost);
			this.childFlag = true;
		}

		if(upCost != 0){
			Puzzle childstate = new Puzzle(state);
			childstate.mvup();
			this.children[2] = new Node(childstate, this.depth + 1, this, Action.up, upCost);
			this.childFlag = true;
		}

		if(downCost != 0){
			Puzzle childstate = new Puzzle(state);
			childstate.mvdown();
			this.children[3] = new Node(childstate, this.depth + 1, this, Action.down, downCost);
			this.childFlag = true;
		}
	};

	Node[] getSorted() {
		//Returns the sorted list of children, or if it has not been sorted yet sorts it and returns it.
		if(this.sortFlag == false){
			this.childSort();
		}
		return this.childSorted;
	}

	void childSort() {
		//Sorts the children according to cost of a node using a basic insertion sort since the list will never be larger the 4.
		this.childSorted = Arrays.copyOf(this.children, 9);
		for (int i = 1; i < 4; i++) {
			int j = i;
			while (j > 0 && compare(childSorted[j-1], childSorted[j]) == 1) { //For loop will also treat a null as if it is > any value.
				Node tmp = childSorted[j - 1];
				childSorted[j - 1] = childSorted[j];
				childSorted[j] = tmp;
				j = j-1;
			}
		}	
		this.sortFlag = true;	
	}

	void printState() {
		//Prints the state.
		System.out.printf("Move: %s; Depth: %d; Cost %d, Total Cost: %d\n", this.move, this.depth, this.cost, this.totalcost);
		System.out.printf("Puzzle Config: ");
		for (int i = 0; i < 9; i++) {
			System.out.printf("%d", this.state.blocks[i]);
		}
		System.out.printf("\n");
	}


	@Override public String toString() {
		//Returns a String version of the puzzle state for use as a hash key and for checking for the solution.
		String intString = "";
		for(Integer number:this.state.blocks){
			intString += number.toString();
		}
		return intString;
	}


	public boolean equals(Node other) {
		//Allows for testing the equality of cost.
		if (this.cost == other.cost) {
			return true;
		}
		else {
			return false;
		}
	}

	public int compare(Node a, Node b) {
		//Implementers comparable based on cost
		//Handles null comparison treats it as larger so that nulls are moved to the end of the array when sorting
		if(a== null && b == null){
			return 0;
		}
		else if(a == null && b != null){
			return 1;
		}
		else if(a != null && b == null){
			return -1;
		}
		else{
			return Integer.compare(a.cost, b.cost);
		}
	}

	@Override
	public int compareTo(Node other) {
		//For use with sort functions in java collections, specifically priority queue
		return Integer.compare(this.cost, other.cost);
	}
}	 