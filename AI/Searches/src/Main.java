import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub
		Puzzle easy = new Puzzle(7, 1, 3, 4, 8, 6, 2, 7, 0, 5);
		Puzzle med = new Puzzle(3, 2, 8, 1, 0, 4, 3, 7, 6, 5);
		Puzzle hard = new Puzzle(4, 5, 6, 7, 4, 0, 8, 3, 2, 1);
		Puzzle diffSelect = new Puzzle();

		int runAgain = 1;
		do{
			@SuppressWarnings("resource")
			Scanner in = new Scanner(System.in);
			System.out.printf("Please Select a search type:\n"
					+ "Enter 1 for Breadth First.\n"
					+ "Enter 2 for Depth First.\n"
					+ "Enter 3 for Iterative Deepening.\n"
					+ "Enter 4 for Uniform-Cost.\n"
					+ "Enter 5 for Best-First.\n"
					+ "Enter 6 for A*1.\n"
					+ "Enter 7 for A*2.\n"
					+ "Enter 8 for A*3.\n\n"			
					);
			String searchType = in.nextLine();

			System.out.printf("Please Select a difficulty:\n"
					+ "Enter 1 for easy.\n"
					+ "Enter 2 for medium.\n"
					+ "Enter 3 for difficult.\n\n"
					);
			String difficulty = in.nextLine();

			switch (difficulty) {
			case "1":diffSelect = easy; break;
			case "2":diffSelect = med; break;
			case "3":diffSelect = hard; break;
			}

			switch(searchType){
			case "1":System.out.printf("Breadth First, Difficulty %s:\n\n", difficulty); Searches.breadthFirst(diffSelect); break;
			case "2":System.out.printf("Depth First, Difficulty %s:\n\n", difficulty); Searches.depthFirst(diffSelect); break;
			case "3":System.out.printf("Iterative Deepening, Difficulty %s:\n\n", difficulty); Searches.iterativeDeep(diffSelect); break;
			case "4":System.out.printf("Uniform Cost, Difficulty %s:\n\n", difficulty); Searches.uniformCost(diffSelect); break;
			case "5":System.out.printf("Best-First, Difficulty %s:\n\n", difficulty); Searches.bestFirst(diffSelect); break;
			case "6":System.out.printf("A1, Difficulty %s:\n\n", difficulty); Searches.A1(diffSelect); break;
			case "7":System.out.printf("A2, Difficulty %s:\n\n", difficulty); Searches.A2(diffSelect); break;
			case "8":System.out.printf("A4, Difficulty %s:\n\n", difficulty); Searches.A3(diffSelect); break;
			}
			
			System.out.printf("Please enter 1 to run again or any other letter to quit.\n");
			runAgain = in.nextInt();
		}while(runAgain == 1);
		
		
		//Below for testing purposes:
	
		//		System.out.printf("Breadth First: Easy.\n");
		//		Searches.breadthFirst(easy);
		//		System.out.printf("Breadth First: Med.\n");
		//		Searches.breadthFirst(med);
		//		System.out.printf("Breadth First: Hard.\n");
		//		Searches.breadthFirst(hard);
		//		
		//		System.out.printf("Depth First: Easy.\n");
		//		Searches.depthFirst(easy);
		//		System.out.printf("Depth First: Med.\n");
		//		Searches.depthFirst(med);
		//		System.out.printf("Depth First: Hard.\n");
		//		Searches.depthFirst(hard);
		//		
		//		System.out.printf("Iterative Deepening: Easy.\n");
		//		Searches.iterativeDeep(easy);
		//		System.out.printf("Iterative Deepening: Med.\n");
		//		Searches.iterativeDeep(med);
		//		System.out.printf("Iterative Deepening: Hard.\n");
		//		Searches.iterativeDeep(hard); 

		//		System.out.printf("Uniform Cost: Easy.\n");
		//		Searches.uniformCost(easy);
		//		System.out.printf("Uniform Cost: Med.\n");
		//		Searches.uniformCost(med);
		//		System.out.printf("Uniform Cost: Hard.\n");
		//		Searches.UniformCost(hard); 

		//		System.out.printf("Best-First: Easy.\n");
		//		Searches.bestFirst(easy);
		//		System.out.printf("Best-First: Med.\n");
		//		Searches.bestFirst(med);
		//		System.out.printf("Best-First: Hard.\n");
		//		Searches.bestFirst(hard); 

		//		System.out.printf("A1: Easy.\n");
		//		Searches.A1(easy);
		//		System.out.printf("A1: Med.\n");
		//		Searches.A1(med);
		//		System.out.printf("A1: Hard.\n");
		//		Searches.A1(hard); 

		//		System.out.printf("A2: Easy.\n");
		//		Searches.A2(easy);
		//		System.out.printf("A2: Med.\n");
		//		Searches.A2(med);
		//		System.out.printf("A1: Hard.\n");
		//		Searches.A2(hard); 

		//		System.out.printf("A3: Easy.\n");
		//		Searches.A3(easy);
		//		System.out.printf("A3: Med.\n");
		//		Searches.A3(med);
		//		System.out.printf("A3: Hard.\n");
		//		Searches.A3(hard); 
	}

}
