import java.util.Arrays;

enum Action {left, right, up, down, start};

public class Puzzle{
	
	static Puzzle solution = new Puzzle(4, 1, 2, 3, 8, 0, 4, 7, 6, 5);
	
	//Enum included in order to make passing commands and accesing children in arrays more easily understood when reading the code.
	Integer blocks[] = new Integer[9];
	int zeroPos = 0; //Marks the location of the zero, the moveable piece.

	public Puzzle() {
		//Default constructor
	}

	public Puzzle(int tl, int tm, int tr, int ml, int mm, int mr, int bl, int bm, int br) {
		//Constructor takes all numbers for the puzzle and then searches for the locaiton of the 0 or moveable block.
		this.blocks[0] = tl;
		this.blocks[1] = tm;
		this.blocks[2] = tr;
		this.blocks[3] = ml;
		this.blocks[4] = mm;
		this.blocks[5] = mr;
		this.blocks[6] = bl;
		this.blocks[7] = bm;
		this.blocks[8] = br;
		for (int i = 0; i < 9; i++) {
			if (blocks[i] == 0) {
				zeroPos = i;
				break;
			}
		}
	}

	public Puzzle(int zeroT, int tl, int tm, int tr, int ml, int mm, int mr, int bl, int bm, int br) {
		//Constructor allows the user to indicate where the 0 block is located.
		this.blocks[0] = tl;
		this.blocks[1] = tm;
		this.blocks[2] = tr;
		this.blocks[3] = ml;
		this.blocks[4] = mm;
		this.blocks[5] = mr;
		this.blocks[6] = bl;
		this.blocks[7] = bm;
		this.blocks[8] = br;
		zeroPos = zeroT;
	}

	public Puzzle(Puzzle clone) {
		this.blocks = Arrays.copyOf(clone.blocks, 9);
		this.zeroPos = clone.zeroPos;
	}

	public Puzzle(Action move, Puzzle parent) {
		//Copy constructor that copies a parent state and then performs the move specified, returning the altered copy.
		Puzzle child = new Puzzle(parent);
		switch (move) {	
		case left:
			child.mvleft();
			break;
		case right:
			child.mvright();
			break;
		case up:
			child.mvup();
			break;
		case down:
			child.mvdown();
			break;
		case start:
			break;
		default:
			break;
		}
	}

	int mvleft() {
		//Moves the 0 block to the left.
		if (this.zeroPos != 0 && this.zeroPos != 3 && this.zeroPos != 6) { //Checks to make sure the 0 is in a postion that can move in this direction.
			swap(zeroPos - 1); //Performs the swap.
		}
		return zeroPos;
	}

	int mvright() {
		//Moves the 0 block to the right in the same manner as above.
		if (this.zeroPos != 2 && this.zeroPos != 5 && this.zeroPos != 8) {
			swap(this.zeroPos + 1);
		}
		return this.zeroPos;
	}
	int mvup() {
		//Moves the 0 block up in the same manner as above.
		if (this.zeroPos > 2) {
			swap(this.zeroPos - 3);
		}
		return this.zeroPos;
	}
	int mvdown() {
		//Moves the 0 block down in the same manner as above.
		if (this.zeroPos < 6) {
			swap(this.zeroPos + 3);
		}
		return this.zeroPos;
	}

	int costleft() {
		//Calculates and returns the cost of a move to the left, if the move is not possible it returns 0.
		int cost = 0;
		if (this.zeroPos != 0 && this.zeroPos != 3 && this.zeroPos != 6) { 
			cost = this.blocks[this.zeroPos-1];
		}
		return cost;
	}

	int costright() {
		//Calculates and returns the cost of a move to the right, if the move is not possible it returns 0.
		int cost = 0;
		if (this.zeroPos != 2 && this.zeroPos != 5 && this.zeroPos != 8) { 
			cost = this.blocks[this.zeroPos + 1];
		}
		return cost;
	}

	int costup() {
		//Calculates and returns the cost of a move up, if the move is not possible it returns 0.
		int cost = 0;
		if (this.zeroPos > 2) { 
			cost = this.blocks[this.zeroPos - 3];
		}
		return cost;
	}

	int costdown() {
		//Calculates and returns the cost of a move down, if the move is not possible it returns 0.
		int cost = 0;
		if (this.zeroPos < 6) { 
			cost = this.blocks[this.zeroPos + 3];
		}
		return cost;
	}

	void swap(int blockPos) {
		//Swaps the selected block with the current locatation of the 0.
		blocks[this.zeroPos] = blocks[blockPos];
		blocks[blockPos] = 0;
		this.zeroPos = blockPos;
	}

	//Comparison operators to allow a puzzle to be compared to the solved state.
	boolean equals(Puzzle b) {
		boolean blockFlag = true;
		for (int i = 0; i < 9; i++) {
			if (this.blocks[i] != b.blocks[i]) {
				blockFlag = false;
			}
		}
		return blockFlag;
	}
}
