import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class hNode implements Comparator<hNode>, Comparable<hNode>{
	
	
	
	//Node for use with heuristic search algs, implements different comparators has heuristic related functions and has an hval to store the value of the heuristic.
	public
	Puzzle state = new Puzzle();
	int depth = 0;
	int cost = 0;
	int totalcost = 0;
	boolean expanded = false;
	boolean childFlag = false;
	boolean sortFlag = false;
	hNode parent = null;
	hNode[] children = {null, null, null, null};
	hNode[] childSorted = {null, null, null, null};
	Action move = Action.start;
	int hval;

	hNode(){
		//Default constructor creates a generic hNode.
	};

	hNode(Puzzle stateT, int depthT, hNode parentT, Action moveT, int costT){ 
		//Constructor for child hNodes.
		this.state = stateT;
		this.depth = depthT; 
		this.parent = parentT; 
		this.move = moveT; 
		this.cost = costT; 
		this.expanded = false;
		if(this.parent != null){
			this.totalcost = costT + parent.totalcost;
		}
		else{
			this.totalcost = costT;
		}
		this.hval = 9;
	}

	hNode(Puzzle stateT) {
		//Constructor for a top hNode
		this.state = stateT; 
	}

	void expand() {
		//Function to expand child hNodes, checks the cost of a move (0= not possible) and then performs the possible moves.
		this.expanded = true;
		int leftCost = this.state.costleft();
		int rightCost = this.state.costright();
		int upCost = this.state.costup();
		int downCost = this.state.costdown();

		if(leftCost != 0){
			Puzzle childstate = new Puzzle(this.state);
			childstate.mvleft();
			this.children[0] = new hNode(childstate, this.depth+1, this, Action.left, leftCost);
			this.children[0].greedyH();
		}

		if(rightCost != 0){
			Puzzle childstate = new Puzzle(state);
			childstate.mvright();
			this.children[1] = new hNode(childstate, this.depth + 1, this, Action.right, rightCost);
			this.childFlag = true;
		}

		if(upCost != 0){
			Puzzle childstate = new Puzzle(state);
			childstate.mvup();
			this.children[2] = new hNode(childstate, this.depth + 1, this, Action.up, upCost);
			this.childFlag = true;
		}

		if(downCost != 0){
			Puzzle childstate = new Puzzle(state);
			childstate.mvdown();
			this.children[3] = new hNode(childstate, this.depth + 1, this, Action.down, downCost);
			this.childFlag = true;
		}
	};

	hNode[] getSorted() {
		//Returns the sorted list of children, or if it has not been sorted yet sorts it and returns it.
		if(this.sortFlag == false){
			this.childSort();
		}
		return this.childSorted;
	}

	void childSort() {
		//Sorts the children according to cost of a hNode using a basic insertion sort since the list will never be larger the 4.
		this.childSorted = Arrays.copyOf(this.children, 9);
		for (int i = 1; i < 4; i++) {
			int j = i;
			while (j > 0 && compare(childSorted[j-1], childSorted[j]) == 1) { //For loop will also treat a null as if it is > any value.
				hNode tmp = childSorted[j - 1];
				childSorted[j - 1] = childSorted[j];
				childSorted[j] = tmp;
				j = j-1;
			}
		}	
		this.sortFlag = true;	
	}

	void greedyH(){
		//Calculates hval based solely on the number of blocks already in place.
		Integer[] solution = {1, 2, 3, 8, 0, 4, 7, 6, 5};
		Integer hcounter = 9;
		for(int i = 0; i < 9; i++){
			if(this.state.blocks[i] == solution[i]){
				hcounter--;
			}			
		}
		this.hval = hcounter;
	}

	void aH(){
		//Calculates hval based on the number of blocks in place plus the total cost of all moves so far.
		Integer[] solution = {1, 2, 3, 8, 0, 4, 7, 6, 5};
		Integer hcounter = 9;
		for(int i = 0; i < 9; i++){
			if(this.state.blocks[i] == solution[i]){
				hcounter--;
			}			
		}
		this.hval = hcounter + this.totalcost;
	}
	
	void manhattanH(){
		//Calculates the hval based upon the number of Manhattan distances between current and correct tile positions plus cost so far.
		List<Integer> solution = Arrays.asList(1, 2, 3, 8, 0, 4, 7, 6, 5);
		//The array array bellow stores the distances between each block based on index, ie the distance between 0 and 8 is nahatDist[0][8] or [8][0]
		Integer[][] manhatDist = {{0,1,2,1,2,3,2,3,4},{1,0,1,2,1,2,3,2,3},{2,1,0,3,2,1,2,3,2},{1,2,3,0,1,2,1,2,3,},{2,1,2,1,0,1,2,1,2},{3,2,1,2,1,0,3,2,1},{2,3,4,1,2,3,0,1,1},{3,2,3,2,1,2,1,0,2},{4,3,2,3,2,1,2,1,0}};
		for(int curIndex = 0; curIndex < 9; curIndex++){
			Integer blockValue = this.state.blocks[curIndex];
			Integer correctIndex = solution.indexOf(blockValue);
			this.hval += (this.totalcost + (manhatDist[curIndex][correctIndex]*blockValue)); //The distance is multiplied by the cost of the block for an approximation of the cost of the move
		}
		
	}
	
	void customH(){
		//Calculates the hval based upon the number of blocks that are more than one swap away from their correct location plus total cost.
		int numOut = 0;
		List<Integer> solution = Arrays.asList(1, 2, 3, 8, 0, 4, 7, 6, 5);
		//The array array bellow stores the distances between each block based on index, ie the distance between 0 and 8 is nahatDist[0][8] or [8][0]
		Integer[][] manhatDist = {{0,1,2,1,2,3,2,3,4},{1,0,1,2,1,2,3,2,3},{2,1,0,3,2,1,2,3,2},{1,2,3,0,1,2,1,2,3,},{2,1,2,1,0,1,2,1,2},{3,2,1,2,1,0,3,2,1},{2,3,4,1,2,3,0,1,1},{3,2,3,2,1,2,1,0,2},{4,3,2,3,2,1,2,1,0}};
		for(Integer curIndex = 0; curIndex < 9; curIndex++){
			Integer blockValue = this.state.blocks[curIndex];
			Integer correctIndex = solution.indexOf(blockValue);
			if(Math.abs(curIndex - manhatDist[curIndex][correctIndex]) > 1){ //Subtracts to verify that it is more than one move.
				numOut++;
			}
			this.hval += (this.totalcost + numOut);

		}
	}


	void printState() {
		//Prints the state.
		System.out.printf("Move: %s; Depth: %d; Cost: %d; Total Cost: %d; H Value: %d\n", this.move, this.depth, this.cost, this.totalcost, this.hval);
		System.out.printf("Puzzle Config: ");
		for (int i = 0; i < 9; i++) {
			System.out.printf("%d", this.state.blocks[i]);
		}
		System.out.printf("\n");
	}


	@Override public String toString() {
		//Returns a String version of the puzzle state for use as a hash key and for checking for the solution.
		String intString = "";
		for(Integer number:this.state.blocks){
			intString += number.toString();
		}
		return intString;
	}


	public boolean equals(hNode other) {
		//Allows for testing the equality of hval and puzzle state.
		if (this.hval == other.hval) {
			return true;
		}
		else {
			return false;
		}
	}

	public int compare(hNode a, hNode b) {
		//Implementers comparable based on cost
		//Handles null comparison treats it as larger so that nulls are moved to the end of the array when sorting
		if(a== null && b == null){
			return 0;
		}
		else if(a == null && b != null){
			return 1;
		}
		else if(a != null && b == null){
			return -1;
		}
		else{
			return Integer.compare(a.hval, b.hval);
		}
	}

	@Override
	public int compareTo(hNode other) {
		//For use with sort functions in java collections, specifically priority queue
		return Integer.compare(this.hval, other.hval);
	}
}	 

