import java.util.Scanner;

public class Game {
	private static Game gameInstance = null;
	GameState board; //the game
	
	//Private constructor
	private Game(){
		this.board = new GameState();
	}
	
	//Singleton
	public static Game getGame(){
		if(gameInstance == null){
			gameInstance = new Game();
		}
		return gameInstance;
	}
	
	//Starts a loop that continues the game until there are no more moves available.
	public void play(){
		Scanner in = new Scanner(System.in);
		System.out.printf("Please select a max number of rounds for the computer to search. (i.e. 1 = 1 human move and 1 computer move.)\n");
		int max = in.nextInt();
		
		CompPlayer ai = new CompPlayer(max*2);
		
		do{
			boolean valid = false;
			//Prints the board
			this.board.printBoard();
				
			//Loop runs until a valid move is entered by the player
			do{
				//Asks for and performs the players move.
				System.out.printf("Please enter 1-9 to select the column you wish to play.\n");
				int move = in.nextInt()-1;
				valid = this.board.move(0, move);
				
				if(valid== false){
					System.out.printf("Please try again.");
				}
				
			}while(valid == false);
			
			//Gets the ai move and performs it.
			int aiMove = ai.nextMove(board);	
			this.board.move(1, aiMove);
			
		}while(!this.board.gameOver()); //Checks that more moves are available.
		
		System.out.printf("The game is over, all positions are filled.\n");
		this.board.printBoard();
		
		in.close();
	}
}
