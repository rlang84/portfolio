import java.util.Random;

public class CompPlayer {
	int maxDepth;
	static int extraInfo = 0; //Used for debugging, if set to 1 will print out the top level children score values as well as indicate when pruning occurs.
	

	public CompPlayer(int max) {
		this.maxDepth = max;
	}

	public int nextMove(GameState board) {
		return minimax(board, this.maxDepth);
	}
	
	//Minimax is made up of 3 functions which call one another recursively. This implementation is based upon the sample pseudo-code provided in the textbook.
	private static int minimax(GameState board, int maxDepth){
		int max = Integer.MIN_VALUE;		
		int maxcol = 0;
		int changeflag = 0;
		int children[] = new int[9];
		for(int i = 0; i < 9; i++){//9 iterations, one for each column that can be chosen.
			GameState nextState = GameFact.buildState(board, 1, i); //Copies the current game state and makes the moves, using the gamefactory
			if(nextState == null){//Checks for full columns 
				continue;
			}
			int maxtmp = minPlay(nextState, maxDepth, Integer.MIN_VALUE, Integer.MAX_VALUE); //Starts a recursion for each of the nine columns
			children[i]= maxtmp;
			if(maxtmp > max){//Looks for highest value path
				max = maxtmp; 
				maxcol = i;//When a new max is found the column is recorded.
				changeflag = 1;
			}
		}
		if(changeflag == 0){
			Random randColumn = new Random(); //Initializes the column selected to a random int, so that it does not select the same column whenever there is not a clear choice.
			maxcol = randColumn.nextInt(9);
		}
		if(extraInfo == 1){System.out.printf("\n%d %d %d %d %d %d %d %d %d\n", children[0], children[1], children[2], children[3], children[4], children[5], children[6], children[7], children[8]);};
		return maxcol;
	}
	//Maximizing funciton
	private static int maxPlay(GameState board, int maxDepth, int alpha, int beta){
		if(maxDepth==0){ //If max depth is hit recursion is stopped.
			return((board.getScore(1)));
		}
		int maxval = Integer.MIN_VALUE; //Sets max default to lowest int
		for(int i = 0; i < 9; i++){ //9 iterations, one for each column that can be chosen.
			GameState nextState = GameFact.buildState(board, 1, i);//Copies the current game state and makes the moves, using the gamefactory
			if(nextState == null){ //Skips iteration if the column is filled.
				continue;
			}
			int returnVal = minPlay(nextState, maxDepth-1, alpha, beta); //Recursive-ish call down to the next min round.
			maxval = Math.max(maxval, returnVal);//Selects the larger between the value returned by the recursion and the stored max.
			if(maxval >= beta){//Stops the loop if greater than beta.
				if(extraInfo == 1){System.out.printf("\nBeta Pruned a Branch\n");}
				break;
			}
			alpha = Math.max(alpha, maxval);//Sets the new alpha. Performing the pruning.
		}	
		return maxval;
	}
	//Minimizing function
	private static int minPlay(GameState board, int maxDepth, int alpha, int beta){
		if(maxDepth==0){//If max depth is hit recursion is stopped.
			return((board.getScore(1)));
		}
		int minval = Integer.MAX_VALUE;//Sets max default to highest int
		for(int i = 0; i < 9; i++){//9 iterations, one for each column that can be chosen.
			GameState nextState = GameFact.buildState(board, 0, i);//Copies the current game state and makes the moves, using the gamefactory
			if(nextState == null){//Skips iteration if the column is filled.
				continue;
			}
			int returnVal = maxPlay(nextState, maxDepth-1, alpha, beta);//Recursive-ish call down to the next min round.
			minval = Math.min(minval, returnVal);//Selects the larger between the value returned by the recursion and the stored max.
			if(minval <= alpha){//Stops the loop if less than alpha. Performing the pruning.
				if(extraInfo == 1){System.out.printf("\nAlpha Pruned a Branch\n");} //Debug info.
				break;
			}
			beta = Math.min(beta, minval);//Sets the new beta.
		}	
		return minval;
	}
}
