
public class GameFact {
	
	//Factory method for gamestate. Allows for deep copy of the state as well as creation and specification of a move in one step.
	//Takes the game state to copy, an int for the moving player 0=human, 1=ai, and a column to drop a piece.
	//If successfull returns the new state, if not returns null, indicating that the column is full and the move is not possible.
	public static GameState buildState(GameState old, int player, int column){
		GameState fresh = new GameState(old);
		if(fresh.move(player, column)==true){
			return fresh;
		}
		else{
			return null;
		}
	}
}
