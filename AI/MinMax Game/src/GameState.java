import java.util.Arrays;

public class GameState {
	int[][] grid; //Column major storage of the game grid.
	int[] playerScore; //Stores both players score [0] = human, [1] = AI
	int[] freeRow; //Stores the location of the first available spot in each column, to prevent the need to search.
	int freeSpaces; //Tracks total number of free spaces to determine when game is over.
	
	//Constructor
	public GameState(){
		this.grid = new int[9][9]; 
		for(int i = 0; i < 9; i++){
			Arrays.fill(this.grid[i], 9); //All spaces of the board are set to 9
		}	
		this.playerScore = new int[]{0,0};
		this.freeRow = new int[]{8,8,8,8,8,8,8,8,8};
		this.freeSpaces = 80;
	}
	
	//Constructor for minimax search.
	public GameState(GameState old){
		this.grid = new int[9][9]; 
		for(int i = 0; i < 9; i++){
			for(int j = 0; j < 9; j++){
				this.grid[i][j] = old.grid[i][j];
			}
		}
		this.playerScore = Arrays.copyOf(old.playerScore, 9);
		this.freeRow = Arrays.copyOf(old.freeRow, 9);
		this.freeSpaces = old.freeSpaces;
	}
	
	public int[][] getGrid(){
		return this.grid;
	}
	
	//Performs the actual moves. Takes the player number and column for drop as input.
	//Returns true is successful and false if invalid.
	public boolean move(int player, int column){
		boolean success = false; //success flag
		if(column < grid.length && column >= 0){ //Checks if valid index.
			if(freeRow[column] >= 0){ //Checks if column is full
				this.grid[column][freeRow[column]] = player; //Changes the number in the board array to that of the player,human = 0, AI = 1. 
				scoreUpdate(column, this.freeRow[column], player); //updates the player's score
				freeRow[column]--; //Updates column
				this.freeSpaces--; //Subtracts from free spaces
				success = true; //Sets success flag to true
			}
		} 
		return success;
	}
	
	//Calculate score based on neighboring boxes.
	public int scoreUpdate(int column, int row, int player){
		int left = column - 1;
		int right = column + 1;
		int up = row - 1;
		int down = row + 1; 
		
		//Calculates score based on left right up and down.
		//The if statements verify that the space exists.
		if(left > 0 && left < 9){
			if(grid[left][row]==player){
				playerScore[player] += 2;
			}
		}
		if(right > 0 && right < 9){
			if(grid[right][row]==player){
				playerScore[player] += 2;
			}
		}
		if(up > 0 && up < 9){
			if(grid[column][up]==player){
				playerScore[player] += 2;
			}
		}
		if(down > 0 && down < 9){
			if(grid[column][down]==player){
				playerScore[player] += 2;
			}
		}
		//Diagonal Box Scoring
		if(left > 0 && left < 9 && up > 0 && up < 9){
			if(grid[left][up]==player){
				playerScore[player] += 1;
			}
		}
		if(right > 0 && right < 9 && down > 0 && down < 9){
			if(grid[right][down]==player){
				playerScore[player] += 1;
			}
		}
		if(right > 0 && right < 9 && up > 0 && up < 9){
			if(grid[right][up]==player){
				playerScore[player] += 1;
			}
		}
		if(left > 0 && left < 9 && down > 0 && down < 9){
			if(grid[left][down]==player){
				playerScore[player] += 1;
			}
		}
		return this.playerScore[player];
	}
	
	//Function to query if any moves are possible, returns false when all 81 slots are filled.
	public boolean gameOver(){
		if(freeSpaces >= 1){
			return false;
		}
		else{
			return true;
		}
	}
	
	//Score getter, returns score for designated player, human = 0, computer = 1, returns null if an invalid player is entered. 
	public Integer getScore(int player){
		if(player != 0 && player != 1){
			return null;
		}
		else{
			return playerScore[player];
		}		
	}
	
	//Loops over the board to print it.
	public void printBoard(){
		for(int i = 0; i < 9; i++){
			for(int j = 0; j < 9; j++){
				if(grid[j][i] == 9){
					System.out.printf(" . ");
				}
				else if(grid[j][i] == 0){
					System.out.printf(" O ");
				}
				else if(grid[j][i] == 1){
					System.out.printf(" X ");
				}
				if(j == 8){
					System.out.printf("\n");
				}
			}
		}
		System.out.printf("\nScore: Player(%d) Computer(%d)", playerScore[0], playerScore[1]);
		String moveMess = (freeSpaces < 0) ? "\nNo Moves Remaining\n" : "\n" + freeSpaces + " Moves Remaining\n";
		System.out.printf(moveMess);
	}
}
