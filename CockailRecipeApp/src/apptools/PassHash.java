package apptools;

import java.security.SecureRandom;

import javax.crypto.*;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

public class PassHash {
	
	//Function hashes a password and returns the hash, the salt must also be provided. Based upon: http://docs.oracle.com/javase/7/docs/technotes/guides/security/crypto/CryptoSpec.html#PBEEx
	public static byte[] hashPass(String password, byte[] salt){
		byte[] hashed = new byte[0];
		PBEKeySpec pbeKeySpec = new PBEKeySpec(password.toCharArray(), salt, 7000, 128);
		try{
			SecretKeyFactory keyFac = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			hashed = keyFac.generateSecret(pbeKeySpec).getEncoded();
		}catch(Exception e){
			e.printStackTrace(); 
		}

		return hashed;
	}
	
	//Gets a salt using the built in secure random. 128 bit salt
	public static byte[] getSalt(){
	      SecureRandom random = new SecureRandom();
	      byte bytes[] = new byte[16];
	      random.nextBytes(bytes);
		return bytes;
		
	}
}
