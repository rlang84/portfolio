package dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.ConcurrentHashMap;

import model.Ingredient;
import model.IngredientType;
import model.User;

public class IngredientDB {
	
	//Retrieves a users saved ingredients
	public static ConcurrentHashMap<String, Ingredient> getUserIngredients(User user){
		DBConnect db = DBConnect.getInstance();
		Connection con = db.getConnection();
		PreparedStatement prep = null;
		ConcurrentHashMap<String, Ingredient> ingredientMap = new ConcurrentHashMap<String, Ingredient>();
		//Query
		String statement = 
				"SELECT * FROM Ingredients"+ 
				"JOIN (SELECT * FROM User_Ingredients"+  
				"WHERE User_Ingredients.user_id = '" + user.getId() + "') as SubQ"+
				"ON (SubQ.ingredient_id = Ingredients.id);";
		try {
			prep = con.prepareStatement(statement);
			ResultSet result = prep.executeQuery();	
			//Reads Results and constructs Ingredient objects for return.
			while (result.next()) {
				String id = result.getString("id");
				String name = result.getString("ingredient_name");
				String type = result.getString("type");
				String description = result.getString("description");		
				Ingredient ingredient = new Ingredient(id, name, IngredientType.valueOf(type), description);
				ingredientMap.put(ingredient.getName(), ingredient);
				
			}
		} catch (SQLException e) {
			System.out.println(e);
			SQL_Utility.closePS(prep);
		}
		db.freeConnection(con);
		return ingredientMap;
	}
	
	//Same as above but takes only the user id as a parameter.
	public static ConcurrentHashMap<String, Ingredient> getUserIngredients(String user_id){
		DBConnect db = DBConnect.getInstance();
		Connection con = db.getConnection();
		PreparedStatement prep = null;
		ConcurrentHashMap<String, Ingredient> ingredientMap = new ConcurrentHashMap<String, Ingredient>();
		//Query
		String statement = 
				"SELECT * FROM Ingredients"+ 
				"JOIN (SELECT * FROM User_Ingredients"+  
				"WHERE User_Ingredients.user_id = '" + user_id + "') as SubQ"+
				"ON (SubQ.ingredient_id = Ingredients.id);";
		try {
			prep = con.prepareStatement(statement);
			ResultSet result = prep.executeQuery();	
			//Reads Results and constructs Ingredient objects for return.
			while (result.next()) {
				String id = result.getString("id");
				String name = result.getString("ingredient_name");
				String type = result.getString("type");
				String description = result.getString("description");		
				Ingredient ingredient = new Ingredient(id, name, IngredientType.valueOf(type), description);
				ingredientMap.put(ingredient.getName(), ingredient);
				
			}
		} catch (SQLException e) {
			System.out.println(e);
			SQL_Utility.closePS(prep);
		}
		db.freeConnection(con);
		return ingredientMap;
	}
	
	//Retrieves all ingredients and returns a hashmap of them for easy searching.
	public static ConcurrentHashMap<String, Ingredient> getIngredientsByType(IngredientType queryType){
		DBConnect db = DBConnect.getInstance();
		Connection con = db.getConnection();
		PreparedStatement prep = null;
		ConcurrentHashMap<String, Ingredient> ingredientMap = new ConcurrentHashMap<String, Ingredient>();
		//Query
		String statement = 
				"SELECT * FROM Ingredients WHERE Ingredients.type = '" + queryType + "');";
		try {
			prep = con.prepareStatement(statement);
			ResultSet result = prep.executeQuery();	
			//Reads Results and constructs Ingredient objects for return.
			while (result.next()) {
				String id = result.getString("id");
				String name = result.getString("ingredient_name");
				String type = result.getString("type");
				String description = result.getString("description");		
				Ingredient ingredient = new Ingredient(id, name, IngredientType.valueOf(type), description);
				ingredientMap.put(ingredient.getName(), ingredient);
				
			}
		} catch (SQLException e) {
			System.out.println(e);
			SQL_Utility.closePS(prep);
		}
		db.freeConnection(con);
		return ingredientMap;
	}
	
	//Retrieves all ingredients of the provided type and returns a list of them.
	public static ConcurrentHashMap<String, Ingredient> getAllIngredients(){
		DBConnect db = DBConnect.getInstance();
		Connection con = db.getConnection();
		PreparedStatement prep = null;
		ConcurrentHashMap<String, Ingredient> ingredientMap = new ConcurrentHashMap<String, Ingredient>();
		//Query
		String statement = 
				"SELECT * FROM Ingredients);";
		try {
			prep = con.prepareStatement(statement);
			ResultSet result = prep.executeQuery();	
			//Reads Results and constructs Ingredient objects for return.
			while (result.next()) {
				String id = result.getString("id");
				String name = result.getString("ingredient_name");
				String type = result.getString("type");
				String description = result.getString("description");		
				Ingredient ingredient = new Ingredient(id, name, IngredientType.valueOf(type), description);
				ingredientMap.put(ingredient.getName(), ingredient);
				
			}
		} catch (SQLException e) {
			System.out.println(e);
			SQL_Utility.closePS(prep);
		}
		db.freeConnection(con);
		return ingredientMap;
	}
	
	//Inserts new user ingredients into the database
	public static String insertUserIngredient(Ingredient ingredient, User user) {
		DBConnect db = DBConnect.getInstance();
		Connection con = db.getConnection();
		PreparedStatement prep = null;
		String recipeStatement = "INSERT INTO User_Ingredients VALUES('" + user.getId() + "', '" + ingredient.getId() + "');";
		String success = "The ingredient was successfully added to your bar.";

		try {
			prep = con.prepareStatement(recipeStatement);
			prep.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e);
			SQL_Utility.closePS(prep);
			success = "There was an error adding this ingredient. Please try again later.";
		}
		db.freeConnection(con);

		return success;
	}

	//Inserts new ingredients into the database
	public static String insertIngredient(Ingredient ingredient) {
		DBConnect db = DBConnect.getInstance();
		Connection con = db.getConnection();
		PreparedStatement prep = null;
		String recipeStatement = "INSERT INTO Ingredients VALUES('" + ingredient.getId() + "', '" + ingredient.getName() + "', '" + ingredient.getType() + "', '" + ingredient.getDescription() + "');";
		String success = "Your ingredinet was successfully created!";

		if(ingredientCheck(ingredient)){
			try {
				prep = con.prepareStatement(recipeStatement);
				prep.executeUpdate();
			} catch (SQLException e) {
				System.out.println(e);
				SQL_Utility.closePS(prep);
				success = "There was an error creating this ingredient. Please try again later.";
			}
			db.freeConnection(con);
		}
		else{
			success = "That ingredient already exists.";
		}
		return success;
	}

	public static void deleteIngredient(Ingredient ingredient, User user) {

	}
	
	//Checks database for any ingredients that already have the stated name.
	private static boolean ingredientCheck(Ingredient ingredient){
		DBConnect db = DBConnect.getInstance();
		Connection con = db.getConnection();
		PreparedStatement prep = null;
		String statement = "SELECT * FROM Ingredients WHERE ingredient_name= '" + ingredient.getName() + "';";
		Boolean validRecord = true;

		try {
			prep = con.prepareStatement(statement);
			ResultSet result = prep.executeQuery();
			//Checks if an item of that name is already stored.
			if(result.next()){
				validRecord = false;
			}
		} catch (SQLException e) {
			System.out.println(e);
			SQL_Utility.closePS(prep);
		}
		db.freeConnection(con);
		return validRecord;
	}
}
