package dbc;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class SQL_Utility {
	public static void closePS(PreparedStatement prep){
		try{
			if(prep != null){
				prep.close();
			}
		} catch (SQLException e) {
			System.out.println(e);
		}
	}
}
