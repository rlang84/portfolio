package dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.*;

public class SearchBean {
	public static String searchRecipes(Ingredient ingredient) {
		DBConnect db = DBConnect.getInstance();
		Connection con = db.getConnection();
		PreparedStatement prep = null;
		String recipeStatement = "INSERT INTO Ingredients VALUES(" + " " + ingredient.getId() + ", '" + ingredient.getName() + "', '" + ingredient.getDescription() + "';";
		String success = "Your ingredinet was successfully created!";

			try {
				prep = con.prepareStatement(recipeStatement);
				prep.executeUpdate();
			} catch (SQLException e) {
				System.out.println(e);
				SQL_Utility.closePS(prep);
				success = "There was an error creating this ingredient. Please try again later.";
			}
			db.freeConnection(con);
		return success;
	}
	
	//Retrieves recipes that the user can make with their saved ingredients.
	public static boolean ingredientMatch(User user){
		DBConnect db = DBConnect.getInstance();
		Connection con = db.getConnection();
		PreparedStatement prep = null;
		boolean success = false;
		String statement = 
				"SELECT * FROM Recipes"+ 
				"JOIN"+  
				"(SELECT COUNT(Recipe_Ingredients.ingredient_id) AS ingredient_count, recipe_id AS user_recipes FROM Recipe_Ingredients JOIN User_Ingredients"+ 
				"ON (User_Ingredients.ingredient_id = Recipe_Ingredients.ingredient_id)"+ 
				"WHERE User_Ingredients.user_id= "+user.getId()+" GROUP BY recipe_id) AS UR"+
				"ON(UR.user_recipes = Recipes.id AND UR.ingredient_count = Recipes.ing_count);";
		try {
			prep = con.prepareStatement(statement);
			ResultSet result = prep.executeQuery();	
			success = true;
		} catch (SQLException e) {
			System.out.println(e);
			SQL_Utility.closePS(prep);
		}
		db.freeConnection(con);
		return success;
	}
}
