package dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

import model.*;

public class UserDB {
	
	//logs in users checking their credentials against the user database, returns their ID if successfull or null if not.
	public static String login(String name, String password){
		DBConnect db = DBConnect.getInstance();
		Connection con = db.getConnection();
		PreparedStatement prep = null;
		String id = null;
		String userQuery = "SELECT salt, passHash FROM Users WHERE user_name = '"+name+"';";
		try{
			prep = con.prepareStatement(userQuery);
			ResultSet result = prep.executeQuery();
			if (result.next()) {
				byte[] storedSalt = result.getBytes("salt");
				byte[] storedHash = result.getBytes("passHash");
				byte[] newHash = apptools.PassHash.hashPass(password, storedSalt);		
				if(Arrays.equals(newHash, storedHash)){
					id = result.getString("id");
				}
			}	
		}catch(SQLException e){
			e.printStackTrace();
		}

		return id;		
	}
	
	//inserts new users into the database
	public static String insertUser(User user) {
		DBConnect db = DBConnect.getInstance();
		Connection con = db.getConnection();
		PreparedStatement prep = null;
		String userStatement = "INSERT INTO Users VALUES(" + " '" + user.getId() + "', '" + user.getName() + "', '" + user.getAuth() + "', '" + user.getEmail() + "', ?, ?);";
		String success = "Your recipe was successfully created!";
		if(userCheck(user)){
			try {
				prep = con.prepareStatement(userStatement);
				prep.setBytes(1, user.getHash());
				prep.setBytes(2, user.getSalt());
				prep.executeUpdate();
			} catch (SQLException e) {
				System.out.println(e);
				SQL_Utility.closePS(prep);
				success = "There was an error creating your account. Please try again later.";
			}
			db.freeConnection(con);
		}
		else{
			success = "A user with that name or email already exists. Please try again with a different name or email.";
		}
		return success;
	}
	
	//Checks database for any users that already have the stated name or email.
	private static boolean userCheck(User user){
		DBConnect db = DBConnect.getInstance();
		Connection con = db.getConnection();
		PreparedStatement prep_name = null;
		PreparedStatement prep_email = null;
		String name_statement = "SELECT * FROM Users WHERE" + " " + "user_name= '" + user.getName() + "';";
		String email_statement = "SELECT * FROM Users WHERE" + " " + "user_name= '" + user.getName() + "';";
		Boolean validRecord = true;

		try {
			prep_name = con.prepareStatement(name_statement);
			prep_email = con.prepareStatement(email_statement);
			ResultSet name_result = prep_name.executeQuery();
			ResultSet email_result = prep_email.executeQuery();
			//Checks if an item of that name or email is already stored.
			if(name_result.next() || email_result.next()){
				validRecord = false;
			}

		} catch (SQLException e) {
			System.out.println(e);
			SQL_Utility.closePS(prep_name);
			SQL_Utility.closePS(prep_email);
		}
		db.freeConnection(con);
		return validRecord;
	}
}
