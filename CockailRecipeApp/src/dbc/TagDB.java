package dbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.Tag;
import model.User;

public class TagDB {
	
	//Inserts new tags into the database
	public static String insertTag(Tag tag) {
		DBConnect db = DBConnect.getInstance();
		Connection con = db.getConnection();
		PreparedStatement prep = null;
		String recipeStatement = "INSERT INTO Tags VALUES(" + " '" + tag.getId() + "', '" + tag.getName() + "', '" + tag.getDescription() + "';";
		String success = "Your tag was successfully created!";

		if(tagCheck(tag)){
			try {
				prep = con.prepareStatement(recipeStatement);
				prep.executeUpdate();
			} catch (SQLException e) {
				System.out.println(e);
				SQL_Utility.closePS(prep);
				success = "There was an error creating this tag. Please try again later.";
			}
			db.freeConnection(con);
		}
		else{
			success = "That tag already exists.";
		}
		return success;
	}
	

	public static void deleteTag(Tag tag, User user) {

	}
	
	//Checks database for any tags by that name
	private static boolean tagCheck(Tag tag){
		DBConnect db = DBConnect.getInstance();
		Connection con = db.getConnection();
		PreparedStatement prep = null;
		String statement = "SELECT * FROM Tags WHERE" + " " + "tag_name= '" + tag.getName() + "';";
		Boolean validRecord = false;

		try {
			prep = con.prepareStatement(statement);
			ResultSet result = prep.executeQuery();
			//Checks if an item of that name is already stored.
			if(result.next()){
				validRecord = true;
			}
		} catch (SQLException e) {
			System.out.println(e);
			SQL_Utility.closePS(prep);
		}
		db.freeConnection(con);
		return validRecord;
	}
}
