package dbc;

import java.sql.*;

import model.*;

//TO DO: Update and delete methods need to be completed. A way to save recipe and ingredient submissions for review before insertion needs to be created as well. However probably not here.

//Class for handling database interaction
public class RecipeDB {

	//Inserts new recipes into the database
	public static String insertRecipe(Recipe recipe, User user) {
		DBConnect db = DBConnect.getInstance();
		Connection con = db.getConnection();
		PreparedStatement prep = null;
		String recipeStatement = "INSERT INTO Recipes VALUES(" + " '" + recipe.getId() + "', '" + recipe.getName() + "', '" + user.getId() + "', '" + recipe.getDescription() + "', " + recipe.getIngCount() + ");";
		String success = "Your recipe was successfully created!";

		if(recipeCheck(recipe)){
			try {
				prep = con.prepareStatement(recipeStatement);
				prep.executeUpdate();
				for(Ingredient ingredient : recipe.getIngredientList()){
					String ingredientStatement = "INSERT INTO Recipe_Ingredients VALUES('" + ingredient.getId() + "', '" + recipe.getId() + "', '" + ingredient.getName() + "');";
					prep = con.prepareStatement(ingredientStatement);
					prep.executeUpdate();
				}
				for(Tag tag : recipe.getTags()){
					String tagStatement = "INSERT INTO Recipe_Tags '" + tag.getId() + "', '" + recipe.getId() + "', '" + tag.getName() + "';";
					prep = con.prepareStatement(tagStatement);
					prep.executeUpdate();
				}
			} catch (SQLException e) {
				System.out.println(e);
				SQL_Utility.closePS(prep);
				success = "There was an error creating your recipe. Please try again later.";
			}
			db.freeConnection(con);
		}
		else{
			success = "A recipe with that name already exists. Please try again with a different name.";
		}
		return success;
	}

	public static void updateRecipe(Recipe recipe, User user) {

	}

	public static void deleteRecipe(Recipe recipe, User user) {

	}

	//Checks database for any recipes that already have the stated name.
	private static boolean recipeCheck(Recipe recipe){
		DBConnect db = DBConnect.getInstance();
		Connection con = db.getConnection();
		PreparedStatement prep = null;
		String statement = "SELECT * FROM Recipes WHERE" + " " + "recipe_name= '" + recipe.getName() + "';";
		Boolean validRecord = true;

		try {
			prep = con.prepareStatement(statement);
			ResultSet result = prep.executeQuery();
			//Checks if an item of that name is already stored.
			if(result.next()){
				validRecord = false;
			}
		} catch (SQLException e) {
			System.out.println(e);
			SQL_Utility.closePS(prep);
		}
		db.freeConnection(con);
		return validRecord;
	}

}
