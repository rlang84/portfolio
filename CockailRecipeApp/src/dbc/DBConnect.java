package dbc;

import java.sql.*;
import javax.naming.InitialContext;
import javax.sql.DataSource;

//Database pool class.
public class DBConnect {
	
	private static DBConnect pool = null;
	private static DataSource dataSource = null;
	
	//Private constructor ensure singleton
	private DBConnect(){
		try{
			InitialContext ic = new InitialContext();
			dataSource = (DataSource) ic.lookup("java:/comp/env/jdbc/recipes_db");
		}catch(Exception e){
			System.out.println(e);
		}
	}
	
	//Checks if pool exists if not instantiates, in either case returns the DBConnect object.
    public static synchronized DBConnect getInstance() {
        if (pool == null) {
            pool = new DBConnect();
        }
        return pool;
    }

    //Returns a connection
    public Connection getConnection() {
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            System.out.println(e);
            return null;
        }
    }

    //Frees the connection specified.
    public void freeConnection(Connection c) {
        try {
            c.close();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }
	
}
