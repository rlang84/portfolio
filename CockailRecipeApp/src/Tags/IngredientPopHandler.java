package Tags;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import dbc.IngredientDB;
import dbc.RecipeDB;
import dbc.UserDB;
import model.*;

public class IngredientPopHandler extends SimpleTagSupport {
	
	public void doTag() throws JspException, IOException{
		
		User ryan = new User("Ryan", "ry@ry.com", "admin", "a");
		UserDB.insertUser(ryan);
		
		Ingredient tequilla = new Ingredient("Tequilla", IngredientType.Spirit, "");
		IngredientDB.insertIngredient(tequilla);
		Ingredient vodka = new Ingredient("Vodka", IngredientType.Spirit, "");
		IngredientDB.insertIngredient(vodka);
		Ingredient bourbon = new Ingredient("Bourbon", IngredientType.Spirit, "");
		IngredientDB.insertIngredient(bourbon);
		Ingredient dark_rum = new Ingredient("Dark Rum", IngredientType.Spirit, "");
		IngredientDB.insertIngredient(dark_rum);
		Ingredient light_rum = new Ingredient("Light Rum", IngredientType.Spirit, "");
		IngredientDB.insertIngredient(light_rum);
		Ingredient gin = new Ingredient("Gin", IngredientType.Spirit, "");
		IngredientDB.insertIngredient(gin);
		Ingredient scotch = new Ingredient("Scotch", IngredientType.Spirit, "");
		IngredientDB.insertIngredient(scotch);
		
		Ingredient sweet_vermouth = new Ingredient("Sweet Vermouth", IngredientType.Liqueur, "");
		IngredientDB.insertIngredient(sweet_vermouth);
		Ingredient triple_sec = new Ingredient("Tripple Sec", IngredientType.Liqueur, "");
		IngredientDB.insertIngredient(triple_sec);
		Ingredient coffee_liq = new Ingredient("Coffe Liqueur", IngredientType.Liqueur, "");
		IngredientDB.insertIngredient(coffee_liq);
		Ingredient melon_liq = new Ingredient("Melon Liqueur", IngredientType.Liqueur, "");
		IngredientDB.insertIngredient(melon_liq);
		Ingredient dry_vermouth = new Ingredient("Dry Vermouth", IngredientType.Liqueur, "");
		IngredientDB.insertIngredient(dry_vermouth);
		
		Ingredient oj = new Ingredient("Orange Juice", IngredientType.Mixer, "");
		IngredientDB.insertIngredient(oj);
		Ingredient lime_juice = new Ingredient("Lime Juice", IngredientType.Mixer, "");
		IngredientDB.insertIngredient(lime_juice);
		Ingredient lemonade = new Ingredient("Lemonade", IngredientType.Mixer, "");
		IngredientDB.insertIngredient(lemonade);
		Ingredient cola = new Ingredient("Cola", IngredientType.Mixer, "");
		IngredientDB.insertIngredient(cola);
		Ingredient cran = new Ingredient("Cranberry Juice", IngredientType.Mixer, "");
		IngredientDB.insertIngredient(cran);
		Ingredient tonic = new Ingredient("Tonic", IngredientType.Mixer, "");
		IngredientDB.insertIngredient(tonic);
		Ingredient coffee = new Ingredient("Coffee", IngredientType.Mixer, "");
		IngredientDB.insertIngredient(coffee);
		Ingredient grenadine = new Ingredient("Grenadine", IngredientType.Mixer, "");
		IngredientDB.insertIngredient(grenadine);
		
		IngredientDB.insertUserIngredient(dark_rum, ryan);
		IngredientDB.insertUserIngredient(tequilla, ryan);
		IngredientDB.insertUserIngredient(lime_juice, ryan);
		IngredientDB.insertUserIngredient(cola, ryan);
		
		Recipe margarita = new Recipe("Margarita");
		margarita.setIngredients(new ArrayList<Ingredient>(Arrays.asList(tequilla, lime_juice, triple_sec)));
		RecipeDB.insertRecipe(margarita ,ryan);
		
		Recipe cubaLibre = new Recipe("Cuba Libre");
		cubaLibre.setIngredients(new ArrayList<Ingredient>(Arrays.asList(cola, lime_juice, dark_rum)));
		RecipeDB.insertRecipe(cubaLibre ,ryan);
		
		Recipe vodkaTonic = new Recipe("Vodka Tonic");
		vodkaTonic.setIngredients(new ArrayList<Ingredient>(Arrays.asList(vodka, tonic)));
		RecipeDB.insertRecipe(vodkaTonic,ryan);
		
		Recipe screwdriver = new Recipe("Screwdriver");
		screwdriver.setIngredients(new ArrayList<Ingredient>(Arrays.asList(vodka, oj)));
		RecipeDB.insertRecipe(screwdriver,ryan);
		
		Recipe whiskeyCoke = new Recipe("Whiskey and Coke");
		whiskeyCoke.setIngredients(new ArrayList<Ingredient>(Arrays.asList(bourbon, cola)));
		RecipeDB.insertRecipe(whiskeyCoke,ryan);
		
		Recipe longIsland = new Recipe("Long Island Iced Tea");
		longIsland.setIngredients(new ArrayList<Ingredient>(Arrays.asList(tequilla, gin, vodka, light_rum, triple_sec)));
		RecipeDB.insertRecipe(longIsland,ryan);
		
		Recipe ginTonic = new Recipe("Gin and Tonic");
		ginTonic.setIngredients(new ArrayList<Ingredient>(Arrays.asList(gin, tonic, lime_juice)));
		RecipeDB.insertRecipe(ginTonic,ryan);
		
		Recipe vodCran = new Recipe("Vodka Cranberry");
		vodCran.setIngredients(new ArrayList<Ingredient>(Arrays.asList(vodka, cran)));
		RecipeDB.insertRecipe(vodCran,ryan);
		
		Recipe teqSun = new Recipe("Tequilla Sunrise");
		teqSun.setIngredients(new ArrayList<Ingredient>(Arrays.asList(tequilla, oj, grenadine)));
		RecipeDB.insertRecipe(teqSun,ryan);
		
		
		
	}
}
