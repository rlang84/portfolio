package model;

import java.io.Serializable;
import java.util.UUID;

public class Tag implements Serializable{

	private static final long serialVersionUID = 2581090678498090101L;
	
	private String name;
	private TagType type;
	private String description;
	private final String id;

	public Tag(String name, TagType type, String description) {
		this.name = name;
		this.type = type;
		this.description = description;
		this.id = UUID.randomUUID().toString();	
	}
	
	public Tag(String name, String id, TagType type, String description) {
		this.name = name;
		this.id = id;
		this.type = type;
		this.description = description;
	}
	
	//Run of the mill getters and setters below.
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TagType getType() {
		return type;
	}

	public void setType(TagType type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getId() {
		return this.id;
	}
	
}
