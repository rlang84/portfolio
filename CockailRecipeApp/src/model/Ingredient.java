package model;

import java.util.UUID;

public class Ingredient {

	private static final long serialVersionUID = -221835871214279795L;
	
	private String name;
	private IngredientType type;
	private String description;
	private final String id;

	public Ingredient(String name, IngredientType type, String description) {
		this.name = name;
		this.type = type;
		this.description = description;
		this.id = UUID.randomUUID().toString();	
	}
	
	public Ingredient(String name, String id, IngredientType type, String description) {
		this.name = name;
		this.id = id;
		this.type = type;
		this.description = description;
	}
	
	//Run of the mill getters and setters below.
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public IngredientType getType() {
		return type;
	}

	public void setType(IngredientType type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getId() {
		return this.id;
	}

}
