package model;

public enum IngredientType {
	Spirit, Mixer, Liqueur, Garnish, Spice, Other
}
