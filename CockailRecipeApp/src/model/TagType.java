package model;

public enum TagType {
	Season, Flavor, Cuisine, Style, Period, Technique, Ingredient, Pairing, Difficulty, Cost
}
