package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class Recipe implements Serializable{

	private static final long serialVersionUID = -1143185025104406084L;
	
	private final String id;
	private final String name;
	private String description;
	private ArrayList<Ingredient> ingredients;
	private ArrayList<Tag> tags;
	private Integer ing_count;
	
	public Recipe(String name){
		this.name = name;
		this.id = UUID.randomUUID().toString();
		this.ingredients = new ArrayList<Ingredient>();
		this.tags = new ArrayList<Tag>();
		this.description = "";
		this.ing_count = 0;
	}
	
	public Recipe(String name, ArrayList<Ingredient> ingredients, ArrayList<Tag> tags, String description){
		this.name = name;
		this.id = UUID.randomUUID().toString();
		this.ingredients = ingredients;
		this.tags = tags;
		this.description = description;
		this.ing_count = ingredients.size();
	}
	
	public Recipe(String name, String id, ArrayList<Ingredient> ingredients, ArrayList<Tag> tags, String description){
		this.name = name;
		this.id = id;
		this.ingredients = ingredients;
		this.tags = tags;
		this.description = description;
		this.ing_count = ingredients.size();
	}
	
	public void addIngredient(Ingredient ingredient){
		this.ingredients.add(ingredient);
		this.ing_count += 1;
	}
	
	public void removeIngredient(Ingredient ingredient){
		if(this.ingredients.remove(ingredient)){
			this.ing_count -= 1;
		}		
	}
	
	public ArrayList<Ingredient> getIngredientList() {
		return this.ingredients;
	}

	public String getId() {
		return id;
	}

	public ArrayList<Ingredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(ArrayList<Ingredient> ingredients) {
		this.ingredients = ingredients;
		this.ing_count = ingredients.size();
	}

	public ArrayList<Tag> getTags() {
		return tags;
	}

	public void setTags(ArrayList<Tag> arrayList) {
		this.tags = arrayList;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}
	
	public Integer getIngCount() {
		return ing_count;
	}
}
