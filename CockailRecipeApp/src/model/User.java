package model;

import java.util.UUID;

public class User {
	
	private final String id;
	private String name;
	private String email;
	private String auth;
	private byte[] salt;
	private byte[] hash;
	
	//Standard constructor with all member variables.
	public User(String name, String email, String auth, byte[] hash, byte[] salt) {
		this.id = UUID.randomUUID().toString();
		this.name = name;
		this.email = email;
		this.auth = auth;
		this.salt = salt;
		this.hash = hash;	
	}
	
	//Constructor that generates passwords and salts for the user. For future implementaiton.
	public User(String name, String email, String auth, String password) {
		this.id = UUID.randomUUID().toString();
		this.name = name;
		this.email = email;
		this.auth = auth;
		this.salt = apptools.PassHash.getSalt();
		this.hash = apptools.PassHash.hashPass(password, this.salt);
	}
	
	//Constructor that generates passwords and salts for the user with customer account type automatically specified.
	public User(String name, String email, String password) {
		this.id = UUID.randomUUID().toString();
		this.name = name;
		this.email = email;
		this.auth = "customer";
		this.salt = apptools.PassHash.getSalt();
		this.hash = apptools.PassHash.hashPass(password, this.salt);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAuth() {
		return auth;
	}

	public void setAuth(String auth) {
		this.auth = auth;
	}

	public String getId() {
		return this.id;
	}

	public byte[] getHash() {
		return hash;
	}

	public void setHash(byte[] new_hash) {
		this.hash = new_hash;
	}

	public byte[] getSalt() {
		return salt;
	}

	public void setSalt(byte[] salt) {
		this.salt = salt;
	}
}
