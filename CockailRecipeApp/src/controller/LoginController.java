package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dbc.UserDB;

//@WebServlet(name = "LoginController", urlPatterns = {"/logincontroller"})
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);

		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String auth = request.getParameter("auth");

		if(username != null && password != null){
			String id = UserDB.login(username, password);
			if(id != null){
				session.setAttribute("username", username);
				session.setAttribute("userId", id);
				session.setAttribute("auth", auth);
				session.removeAttribute("errorMsg");
				response.sendRedirect("index.jsp");
			}
		}
		else{
			session.setAttribute("errorMsg", "The username or password entered is incorrect.");
			response.sendRedirect("login.jsp");
		}
	}

}
