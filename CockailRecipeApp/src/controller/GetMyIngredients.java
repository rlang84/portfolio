package controller;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Ingredient;
import dbc.IngredientDB;

//Retrieves the user's saved ingredients and packages the names in XML for the javascript. 
@WebServlet("/GetMyIngredients")
public class GetMyIngredients extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		String id = (String) session.getAttribute("id");

		ConcurrentHashMap<String, Ingredient> ingredients = IngredientDB.getUserIngredients(id);
		
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        
        //Converts the search into XML
        if(!ingredients.isEmpty()){
        	response.getWriter().write("<ingredients>");
            for(Ingredient ingredient : ingredients.values()){
            	response.getWriter().write("<ingredientName>" + ingredient.getName() + "</ingredientName>");
            } 
            response.getWriter().write("</ingredients>");
        }
        else{
        	response.setStatus(HttpServletResponse.SC_NO_CONTENT);
        }

		
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request, response);
	}

}
