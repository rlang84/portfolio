package controller;

import java.io.IOException;

import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Ingredient;
import model.IngredientType;
import dbc.IngredientDB;

@WebServlet("/GetIngredientsByType")
public class GetIngredientsByType extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String typeString = (String) request.getParameter("type"); 
		
		try{
			IngredientType type = IngredientType.valueOf(typeString);
			ConcurrentHashMap<String, Ingredient> ingredients = IngredientDB.getIngredientsByType(type);			
	        response.setContentType("text/xml");
	        response.setHeader("Cache-Control", "no-cache");
	        //Converts the search into XML
	        if(!ingredients.isEmpty()){
	        	response.getWriter().write("<ingredients>");
	            for(String ingredient : ingredients.keySet()){
	            	response.getWriter().write("<ingredientName>" + ingredient + "</ingredientName>");
	            } 
	            response.getWriter().write("</ingredients>");
	        }
	        else{
	        	response.setStatus(HttpServletResponse.SC_NO_CONTENT);
	        }
		}
		catch(IllegalArgumentException e){
			
		}
	

        


		
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request, response);
	}

}
