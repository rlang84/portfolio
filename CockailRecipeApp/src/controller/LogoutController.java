package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dbc.UserDB;

@WebServlet(name = "LoginController", urlPatterns = {"/logincontroller"})
public class LogoutController extends HttpServlet{
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		session.removeAttribute("username");
		session.removeAttribute("auth");
		session.invalidate();
		response.sendRedirect("index.jsp");
	}

}

