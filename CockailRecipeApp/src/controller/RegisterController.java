package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.User;
import dbc.UserDB;

@WebServlet(name = "RegisterController", urlPatterns = {"/registercontroller"})
public class RegisterController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		String valid = "";
		
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String email = request.getParameter("email");

		if(username != null && password != null && email != null){
			User user = new User(username, email, password);
			valid = UserDB.insertUser(user);
			if(valid.equals("Your recipe was successfully created!")){
				session.setAttribute("username", user.getName());
				session.setAttribute("auth", user.getAuth());
				response.sendRedirect("index.jsp");
				session.removeAttribute("errorMsg");
			}
			else{
				session.setAttribute("errorMsg", valid);
				response.sendRedirect("register.jsp");
			}
		}
		else{
			session.setAttribute("errorMsg", "There was a problem creating your account.");
			response.sendRedirect("register.jsp");
		}
	}

}